package com.hdscorp.cms.handlers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ValueMap;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceReference;
import org.osgi.service.cm.ConfigurationAdmin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hdscorp.cms.util.PathResolver;

@Component(immediate = true, metatype = true)
@Properties({
		@org.apache.felix.scr.annotations.Property(label = "Configure PDF Seach Path", description = "Configure PDF Seach Path", name = "dampdfpath", value = "/content/dam/public/en_us/pdfs") })
public class PdfListingHandler {

	public static final Logger LOG = LoggerFactory.getLogger(PdfListingHandler.class);

	public List<Map<String, String>> searchAllPDFAssets(SlingHttpServletRequest request, ValueMap props,
			String domainVal) {
		LOG.info("Entry into searchAllPDFAssets method");
		Session session = request.getResourceResolver().adaptTo(Session.class);
		QueryManager qm;
		List<Map<String, String>> list = new ArrayList<Map<String, String>>();
		try {
			BundleContext btx = FrameworkUtil.getBundle(PdfListingHandler.class).getBundleContext();
			ServiceReference sr = btx.getServiceReference(ConfigurationAdmin.class.getName());
			ConfigurationAdmin config = (ConfigurationAdmin) btx.getService(sr);
			if (config != null) {
				//String hostAddress = request.getScheme() + "://" + request.getHeader("host").toString();
				// Configuration pdfConfig =
				// config.getConfiguration("com.hdscorp.cms.handlers.PdfListingHandler");
				String damPdfPath = props.get("pdfRootPath", String.class);// pdfConfig.getProperties().get("dampdfpath").toString();
				Boolean enabled = props.get("pdfListEnable", Boolean.class);
				String[] excludeList = props.get("exclude", String[].class);
				String excludePath = props.get("exclude", String.class);
				StringBuilder excludeListQuery = new StringBuilder();
				String shortPath = null;
				if (excludeList != null) {
					for (int i = 0; i < excludeList.length; i++) {
						excludeListQuery.append(" AND NOT ISDESCENDANTNODE(s, '" + excludeList[i] + "') ");
					}
				} else if (excludePath != null && !excludePath.equals("")) {
					excludeListQuery.append(" AND NOT ISDESCENDANTNODE(s, '" + excludePath + "') ");
				}
				LOG.info("DAM PDF Path : " + damPdfPath + " Enabled : " + enabled);
				if (enabled) {
					String pdfQuery = "SELECT * FROM [dam:Asset] AS s WHERE ISDESCENDANTNODE(s, '" + damPdfPath + "')"
							+ excludeListQuery.toString() + " AND [jcr:content/metadata/dc:format]='application/pdf'";
					qm = session.getWorkspace().getQueryManager();
					Query query = qm.createQuery(pdfQuery, Query.JCR_SQL2);
					LOG.debug("Search Query : " + query.getStatement());
					QueryResult qr = query.execute();
					NodeIterator ni = qr.getNodes();
					while (ni.hasNext()) {
						Node assetNode = ni.nextNode();
						Map<String, String> map = new HashMap<String, String>();
						if (!assetNode.getPath().contains("/subassets/")) {
							shortPath = PathResolver.getShortURLPath(assetNode.getPath());

							map.put("path", PathResolver.getAbsoluteDomainUrl(shortPath, domainVal));
						}
						list.add(map);
					}
				}
			} else {
				LOG.error("Service Reference Error");
			}
		} catch (RepositoryException e) {
			LOG.error("Error in executing PDF search query : ", e);
		} catch (Exception e) {
			LOG.error("Exception : ", e);
		}
		LOG.info("Exit from searchAllPDFAssets method");
		return list;
	}
}
