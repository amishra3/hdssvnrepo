package com.hdscorp.cms.filters;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.PropertyUnbounded;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingFilter;
import org.apache.felix.scr.annotations.sling.SlingFilterScope;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceNotFoundException;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.acs.commons.errorpagehandler.ErrorPageHandlerService;
import com.day.cq.wcm.api.WCMMode;
import com.hdscorp.cms.config.HdsCorpGlobalConfiguration;
import com.hdscorp.cms.constants.ServiceConstants;
import com.hdscorp.cms.util.HdsCorpCommonUtils;

@SlingFilter(
        label = "HDS CORP Asset Gating Filter",
        description = "HDS CORP Asset Gating Filter to check & handle gated assets",
        metatype = true,
        generateComponent = true, // True if you want to leverage activate/deactivate or manage its OSGi life-cycle
        generateService = true, // True; required for Sling Filters
        order = 0, // The smaller the number, the earlier in the Filter chain (can go negative);
                    // Defaults to Integer.MAX_VALUE which push it at the end of the chain
        scope = SlingFilterScope.REQUEST) // REQUEST, INCLUDE, FORWARD, ERROR, COMPONENT (REQUEST, INCLUDE, COMPONENT)
@Properties({
	@Property(name = ServiceConstants.GATING_LANDING_PAGE, description = "Provide geo specific or domain specific gating landing page. Ex -localhost_4502:/content/hdscorp/en_us/news-insights/resources/gated-detail.html,/en-us/:/content/hdscorp/en_us/news-insights/resources/gated-detail.html,/de-de/:/content/hdscorp/de_de/news-insights/resources/gated-detail.html", value = "", unbounded = PropertyUnbounded.ARRAY),								
	@Property(name = ServiceConstants.GEO_ERROR_LANDING_PAGE, description = "Provide tenenat specific error page. Ex -localhost_4502:/content/hdscorp/en_us/errors/404.html,/en-us/:/content/hdscorp/en_us/errors/404.html,/de-de/:/content/hdscorp/de-de/errors/404.html", value = "", unbounded = PropertyUnbounded.ARRAY)
})

public class AssetGatingFilter implements Filter {
    private static final Logger log = LoggerFactory.getLogger(AssetGatingFilter.class);

    @Reference
    ErrorPageHandlerService errorPageHandlerService ;
    
    private String[] assetGatingLandingPages;
    private HashMap<String, String> assetGatingLandingPagesMap;
    
    private String[] errorLandingPages;
    private HashMap<String, String> errorLandingPagesMap;
    
	@Activate
	protected void activate(ComponentContext componentContext) {
		log.info("INSIDE THE ACTIVATE METHOD IN THE FILTER");		
		HdsCorpCommonUtils commonUtils = new HdsCorpCommonUtils();
		this.assetGatingLandingPages = commonUtils.getPropertyAsArray(componentContext.getProperties().get(ServiceConstants.GATING_LANDING_PAGE));
		this.errorLandingPages = commonUtils.getPropertyAsArray(componentContext.getProperties().get(ServiceConstants.GEO_ERROR_LANDING_PAGE));
		try {
			this.assetGatingLandingPagesMap = (HashMap<String, String>) Arrays.asList(this.assetGatingLandingPages)
												.stream()
												.map(elem -> elem.split(":"))
												.collect(Collectors.toMap(e -> e[0], e -> e[1]));
			this.errorLandingPagesMap = (HashMap<String, String>) Arrays.asList(this.errorLandingPages)
					.stream()
					.map(elem -> elem.split(":"))
					.collect(Collectors.toMap(e -> e[0], e -> e[1]));
			
			log.info("Config Array to map conversion finished.");
		} catch (Exception ex) {
			log.error("Exception while populating the gating path & error page path maps - "+ex.getMessage());
		}
	}

    
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        // Usually, do nothing

    }

    @Override
    public final void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException,
            ServletException {
    	
		if (!(request instanceof SlingHttpServletRequest) || !(response instanceof SlingHttpServletResponse)) {
			// Not a SlingHttpServletRequest/Response, so ignore.
			chain.doFilter(request, response);
			return;
		}
    	
		final SlingHttpServletResponse slingResponse = (SlingHttpServletResponse) response;
		final SlingHttpServletRequest slingRequest = (SlingHttpServletRequest) request;
		String refererString = slingRequest.getHeader("Referer") ;
    	try {
			
			String pdfPath= slingRequest.getRequestURI();
			if(isGatingApplicable(pdfPath)){
				
				String gatingParam = (String)HdsCorpGlobalConfiguration.getPropertyValue(HdsCorpGlobalConfiguration.ASSET_GATING_SUCCESS_QUERY_PARAMETER);
				String refferalQueryParam = slingRequest.getParameter("Referer");
				
				String gatingParamVal = slingRequest.getParameter(gatingParam);
				boolean isGated = HdsCorpCommonUtils.isGated(pdfPath, slingRequest) ;
				if(isGated && !HdsCorpCommonUtils.checkValidReferer(refererString, gatingParamVal)){
					String targetURL = getGatingLandingPagePath(pdfPath, (!StringUtils.isBlank(refererString) ? refererString : refferalQueryParam) ) ;
					log.debug("==========PDF is GATED ============Redirecting to "+targetURL);
					// Set the Cache-Control and Expires header
					slingResponse.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
					slingResponse.setHeader("Dispatcher", "no-cache");
					slingResponse.setHeader("Pragma", "no-cache"); // HTTP 1.0.
					slingResponse.setHeader("Expires", "0"); // Proxies.
					slingResponse.setContentType("text/html");
					
					slingRequest.setAttribute("pdfPath", pdfPath);
					slingRequest.getRequestDispatcher(targetURL).forward(request,response);
//					HttpServletResponse httpResponse = (HttpServletResponse) response;
//					httpResponse.sendRedirect(PathResolver.getShortURLPath(targetURL));
					return;
				}else{
					if(isGated){

						log.debug("==========PDF is GATED BUT FORM HAS BEEN FILED============");
						// Set the Cache-Control and Expires header
						slingResponse.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
						slingResponse.setHeader("Dispatcher", "no-cache");
						slingResponse.setHeader("Pragma", "no-cache"); // HTTP 1.0.
						slingResponse.setHeader("Expires", "0"); // Proxies.
						slingResponse.setContentType("text/html");

					}
					log.debug("==========Skipping the Filter==========");
					chain.doFilter(request, response);
					return;
				}

			}else{
				chain.doFilter(request, response);
				return;				
			}			
    		
		} catch (ResourceNotFoundException ex) {
			
			 if (errorPageHandlerService != null && errorPageHandlerService.isEnabled()) {
			        // Check for and handle 404 Requests properly according on Author/Publish
			        if (errorPageHandlerService.doHandle404(slingRequest, slingResponse)) {
			        	String path = getErrorLandingPagePath(slingRequest.getRequestURI(),refererString);
			        	if(StringUtils.isBlank(path)){
			        		path = errorPageHandlerService.findErrorPage(slingRequest, slingRequest.getResource());
			            }
			            if (path != null) {
			            	if(path.contains("500")) {
			            		path = path.replace("500", "404");
			            	}else if(!path.contains("404")){
			            		path = path.replace(".html", "/404.html");
			            	}
			            	log.debug("Request path was -  "+slingRequest.getRequestURI());
			            	log.debug("error page path=======**"+path);
			                slingResponse.setStatus(404);
			                slingResponse.setContentType("text/html");
			                errorPageHandlerService.includeUsingGET(slingRequest, slingResponse, path);
			                return;
			            }
			        }
			    }
			
			log.error("Asset Gating Filter ResourceNotFoundException Error Block - " + ex.getMessage()+" for the following path --- "+((SlingHttpServletRequest)request).getRequestURI());
		} catch (Exception ex) {
			log.error("Asset Gating Filter Error Block - " + ex.getMessage()+" for the following path --- "+((SlingHttpServletRequest)request).getRequestURI());
		}
        
        // Finally, proceed with the the Filter chain
    	chain.doFilter(request, response);
    }

    private boolean isGatingApplicable(String pdfPath) {
    	boolean returnFlag = false ;
    	
    	if(pdfPath.contains("/editor") || pdfPath.contains("/cf") || pdfPath.contains("/editor.html")){
    		return returnFlag;
    	}
    			
    	if((pdfPath.toLowerCase().endsWith(".pdf"))){
    		returnFlag = true ;
    		return returnFlag;
    	}
    	
    	if(pdfPath.toLowerCase().contains("/ext/")){
    		returnFlag = true ;
    		return returnFlag;
    	}
        	
    	return returnFlag;
    }
    
    
    private String getGatingLandingPagePath(String requestPath, String referrerString) {
    	String referrer = referrerString ;
    	if(!StringUtils.isBlank(referrer)){
    		referrer = referrerString.replaceFirst("http://","").replaceFirst("https://","") ;
    		referrer = referrer.replaceFirst(":", "_");
    	}
    	
    	//Initialize it to default
		String forardPath = (String)HdsCorpGlobalConfiguration.getPropertyValue(HdsCorpGlobalConfiguration.ASSET_GATING_FORM_PATH);
		int matchLength = 0;
		Iterator it = assetGatingLandingPagesMap.entrySet().iterator();
		if(!StringUtils.isBlank(referrer)){
			while (it.hasNext()) {
			       Map.Entry pair = (Map.Entry)it.next();
			       String keyInstance = pair.getKey().toString();
			       if(referrer.startsWith(keyInstance)){
			    	   if(keyInstance.length() > matchLength){
			    		   matchLength = keyInstance.length() ;
			    		   forardPath = pair.getValue().toString() ;
			    	   }
			       }
			}
		}
		//If NO match found with referrer, then look for request path
		if(matchLength == 0){
			while (it.hasNext()) {
			       Map.Entry pair = (Map.Entry)it.next();
			       String keyInstance = pair.getKey().toString();
			       if(requestPath.startsWith(keyInstance)){
			    	   if(keyInstance.length() > matchLength){
			    		   matchLength = keyInstance.length() ;
			    		   forardPath = pair.getValue().toString() ;
			    	   }
			       }
			}			
		}
		return forardPath;
	}

    private String getErrorLandingPagePath(String requestPath, String referrerString) {
    	String referrer = referrerString ;
    	if(!StringUtils.isBlank(referrer)){
    		referrer = referrerString.replaceFirst("http://","").replaceFirst("https://","") ;
    		referrer = referrer.replaceFirst(":", "_");
    	}
    	
    	//Initialize it to default
		String forardPath = "";
		int matchLength = 0;
		Iterator it = errorLandingPagesMap.entrySet().iterator();
		if(!StringUtils.isBlank(referrer)){
			while (it.hasNext()) {
			       Map.Entry pair = (Map.Entry)it.next();
			       String keyInstance = pair.getKey().toString();
			       if(referrer.startsWith(keyInstance)){
			    	   if(keyInstance.length() > matchLength){
			    		   matchLength = keyInstance.length() ;
			    		   forardPath = pair.getValue().toString() ;
			    	   }
			       }
			}
		}
	
		//If NO match found with referrer, then look for request path
		if(matchLength == 0){
			while (it.hasNext()) {
			       Map.Entry pair = (Map.Entry)it.next();
			       String keyInstance = pair.getKey().toString();
			       if(requestPath.startsWith(keyInstance)){
			    	   if(keyInstance.length() > matchLength){
			    		   matchLength = keyInstance.length() ;
			    		   forardPath = pair.getValue().toString() ;
			    	   }
			       }
			}
		}
		return forardPath;
	}
    
    @Override
    public void destroy() {
        // Usually, do Nothing
    }
}