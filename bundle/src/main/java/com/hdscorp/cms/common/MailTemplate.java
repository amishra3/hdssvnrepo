package com.hdscorp.cms.common;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Enumeration;

import javax.jcr.Binary;
import javax.jcr.Node;
import javax.jcr.Property;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.nodetype.NodeType;
import javax.mail.Header;
import javax.mail.MessagingException;
import javax.mail.internet.InternetHeaders;

import org.apache.commons.io.IOUtils;
import org.apache.commons.io.input.CountingInputStream;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.StrLookup;
import org.apache.commons.lang3.text.StrSubstitutor;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MailTemplate {

	private static final String HEADER_TO = "To";
	private static final String HEADER_CC = "CC";
	private static final String HEADER_BCC = "BCC";
	private static final String HEADER_REPLYTO = "Reply-To";
	private static final String HEADER_FROM = "From";
	private static final String HEADER_SUBJECT = "Subject";
	private static final String HEADER_BOUNCETO = "Bounce-To";
	private static final String[] PRIMARY_HEADERS = { "To", "CC", "BCC", "Reply-To", "From", "Subject", "Bounce-To" };
	private static final Logger log = LoggerFactory.getLogger(MailTemplate.class);
	private String message;

	public MailTemplate(InputStream inputStream, String encoding)
			throws IOException {
		if (null == inputStream) {
			throw new IllegalArgumentException("input stream may not be null");
		}
		InputStreamReader reader = new InputStreamReader(inputStream, StringUtils.defaultIfEmpty(encoding, "utf-8"));

		StringWriter writer = new StringWriter();
		IOUtils.copy(reader, writer);
		this.message = writer.toString();
	}

	public <T extends HtmlEmail> T getEmail(StrLookup lookup, Class<T> type)
			throws IOException, MessagingException, EmailException {
		if (null == lookup) {
			throw new IllegalArgumentException("lookup may not be null");
		}
		StrSubstitutor substitutor = new StrSubstitutor(lookup);
		String source = substitutor.replace(this.message);
		CountingInputStream in = new CountingInputStream(new ByteArrayInputStream(source.getBytes("iso-8859-1")));
		InternetHeaders headers = new InternetHeaders(in);
		T email = null;
		try {
			Constructor<T> constructor = type.getConstructor(new Class[0]);
			email = constructor.newInstance(new Object[0]);
			String body = source;//.substring(in.getCount());
			log.debug("Content : " + body);
			email.setHtmlMsg(body);
			Enumeration primaryHeaders = headers.getMatchingHeaders(PRIMARY_HEADERS);
			while (primaryHeaders.hasMoreElements()) {
				Header header = (Header)primaryHeaders.nextElement();
				String name = header.getName();
				String value = header.getValue();
				if (null != value) {
					if ("To".equalsIgnoreCase(name)) {
						email.addTo(value);
					} else if ("CC".equalsIgnoreCase(name)) {
						email.addCc(value);
					} else if ("BCC".equalsIgnoreCase(name)) {
						email.addBcc(value);
					} else if ("Reply-To".equalsIgnoreCase(name)) {
						email.addReplyTo(value);
					} else if ("From".equalsIgnoreCase(name)) {
						email.setFrom(value);
					} else if ("Subject".equalsIgnoreCase(name)) {
						email.setSubject(value);
					} else if ("Bounce-To".equalsIgnoreCase(name)) {
						email.setBounceAddress(value);
					}	
				}
				else {
					log.warn("got empty primary header [{}].", name);
				}
			}
			Enumeration secondaryHeaders = headers.getNonMatchingHeaders(PRIMARY_HEADERS);
			while (secondaryHeaders.hasMoreElements()) {
				Header header = (Header)secondaryHeaders.nextElement();
				String name = header.getName();
				String value = header.getValue();
				if (null != value) {
					email.addHeader(name, value);
				} else {
					log.warn("got empty secondary header [{}].", name);
				}
			}
		}
		catch (NoSuchMethodException e) {
			log.error("Exception in NoSuchMethodException ",e);
		}catch (InvocationTargetException e) {
			log.error("Exception in InvocationTargetException ",e);
		}catch (InstantiationException e) {
			log.error("Exception in InstantiationException ",e);
		}catch (IllegalAccessException e) {
			log.error("Exception in IllegalAccessException ",e);
		}
		return email;
	}

	public static MailTemplate create(String path, Session session)
	{
		if (StringUtils.isBlank(path)) {
			throw new IllegalArgumentException("path may not be null or empty");
		}
		if (null == session) {
			throw new IllegalArgumentException("session may not be null");
		}
		InputStream is = null;
		try {
			if (session.itemExists(path)) {
				Node node = session.getNode(path);
				if ("nt:file".equals(node.getPrimaryNodeType().getName())) {
					Node content = node.getNode("jcr:content");
					String encoding = content.hasProperty("jcr:encoding") ? content.getProperty("jcr:encoding").getString() : "utf-8";
					is = content.getProperty("jcr:data").getBinary().getStream();
					log.debug("loaded template [{}].", path);

					return new MailTemplate(is, encoding);
				}
				throw new IllegalArgumentException("provided path does not point to a nt:file node");
			}
		}
		catch (RepositoryException e) {
			log.error("error creating message template: ", e);
		}
		catch (IOException e) {
			log.error("error creating message template: ", e);
		}
		finally {
			IOUtils.closeQuietly(is);
		}
		return null;
	}
}