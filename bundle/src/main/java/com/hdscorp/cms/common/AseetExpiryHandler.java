package com.hdscorp.cms.common;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.jcr.LoginException;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.Value;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.StrLookup;
import org.apache.commons.mail.HtmlEmail;
import org.apache.felix.scr.annotations.Component;
import org.apache.jackrabbit.api.security.user.Authorizable;
import org.apache.jackrabbit.api.security.user.UserManager;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.sling.jcr.api.SlingRepository;

import com.day.cq.mailer.MessageGateway;
import com.day.cq.mailer.MessageGatewayService;

@Component(immediate=true,metatype=true)
public class AseetExpiryHandler {

	public static final Logger LOG = LoggerFactory.getLogger(AseetExpiryHandler.class);	

	public static void searchExpiringAssets(MessageGatewayService messageGatewayService, MessageGateway<HtmlEmail> messageGateway, SlingRepository repository, 
			ResourceResolver resourceResolver,String damPath, String notifyType, List<InternetAddress> emailRecipients,
			String hostAddress, String emailTemplatePath) throws ParseException{
		String defaultWorkspace = repository.getDefaultWorkspace();		
		Session session = null;
			
		
		Map<String,Map<String,String>> assetAndEmail = new HashMap<String,Map<String,String>>();
		try {
			session = repository.loginAdministrative(defaultWorkspace);			

			SimpleDateFormat outsdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");

			Calendar currentTimeCal = Calendar.getInstance();
			if(!notifyType.equals("1")){
				currentTimeCal.add(Calendar.DAY_OF_MONTH, +Integer.valueOf(notifyType)-1);
			}else{
				currentTimeCal.add(Calendar.DAY_OF_MONTH, +Integer.valueOf(notifyType));
			}
			currentTimeCal.set(Calendar.HOUR_OF_DAY, 0);
			currentTimeCal.set(Calendar.MINUTE, 0);
			currentTimeCal.set(Calendar.SECOND, 0);
			currentTimeCal.set(Calendar.MILLISECOND, 000);
			Calendar expiryTimeCal = Calendar.getInstance();
			if(!notifyType.equals("1")){
				expiryTimeCal.add(Calendar.DAY_OF_MONTH, +Integer.valueOf(notifyType)-1);
			}else{
				expiryTimeCal.add(Calendar.DAY_OF_MONTH, +Integer.valueOf(notifyType));
			}
			expiryTimeCal.set(Calendar.HOUR_OF_DAY, 23);
			expiryTimeCal.set(Calendar.MINUTE, 59);
			expiryTimeCal.set(Calendar.SECOND, 59);
			expiryTimeCal.set(Calendar.MILLISECOND, 999);

			String startDateTime = outsdf.format(currentTimeCal.getTime());
			startDateTime = startDateTime.substring(0,startDateTime.length()-2)+":"+startDateTime.substring(startDateTime.length()-2,startDateTime.length());
			LOG.info("Time Stamp : " +startDateTime);
			String endDateTime = outsdf.format(expiryTimeCal.getTime());
			endDateTime = endDateTime.substring(0,endDateTime.length()-2)+":"+endDateTime.substring(endDateTime.length()-2,endDateTime.length());

			String searchQuery = "SELECT * FROM [dam:AssetContent] as s WHERE ISDESCENDANTNODE(s, '"+damPath+"') AND s.[metadata/prism:expirationDate] IS NOT NULL AND s.[metadata/prism:expirationDate]  >= CAST('"+startDateTime+"' AS DATE) AND s.[metadata/prism:expirationDate]  <= CAST('"+endDateTime+"' AS DATE)";

			QueryManager qm = session.getWorkspace().getQueryManager();
			Query query = qm.createQuery(searchQuery, Query.JCR_SQL2);
			LOG.info("Search Query : " + query.getStatement());
			QueryResult result = query.execute();
			NodeIterator nit = result.getNodes();
			int resultCount = 0;
			while(nit.hasNext()){
				StringBuilder sb = new StringBuilder();
				Node assetcontent = (Node) nit.nextNode();
				if(assetcontent.hasNode("metadata")){
						Node child = assetcontent.getNode("metadata");
						if(child.hasProperty("prism:expirationDate")){
							List<String> list = new ArrayList<String>();
							list.add(child.getParent().getPath());
							list.add(child.getParent().getName());
							list.add(child.getProperty("prism:expirationDate").getString());
							Date offTime = child.getProperty("prism:expirationDate").getDate().getTime();	
							SimpleDateFormat outOffTime = new SimpleDateFormat("dd MMM yyyy");
							String expiryTime = outOffTime.format(offTime);
							LOG.info("Time Zone Off Set : "+offTime.getTimezoneOffset());
							LOG.info("Start Date : " + startDateTime +" End Date : " + endDateTime+ " Time Zone : " +expiryTimeCal.getTimeZone());
							String assetTitle = "";
						if(assetcontent.hasNode("metadata")){
							Node metaData = assetcontent.getNode("metadata");
							if(metaData.hasProperty("dc:title")){
								if(metaData.getProperty("dc:title").isMultiple()){							
									assetTitle += metaData.getProperty("dc:title").getValues()[0].getString();
								}else{
									assetTitle = metaData.getProperty("dc:title").getString();
								}
							}
						}
						String ownerMailId = "";
						if(assetcontent.hasNode("metadata")){
							Node metaData = assetcontent.getNode("metadata");
							if(metaData.hasProperty("dc:businesowneremail")){
								ownerMailId = metaData.getProperty("dc:businesowneremail").getString();								
							}
						}						
						if(assetTitle.equals("")){
							assetTitle = assetcontent.getParent().getName();
						}
						String contentAuthorName = "";
						Node asset = assetcontent.getParent();
						if(asset.hasProperty("jcr:createdBy")){
							contentAuthorName = asset.getProperty("jcr:createdBy").getString();
							UserManager userManager = resourceResolver.adaptTo(UserManager.class);
							Authorizable auth = userManager.getAuthorizable(contentAuthorName);
							Value[] propArray = auth.getProperty("profile/email");
		
						}	
						sb.append("<tr>");
						sb.append("<td  style=\"font-family:Arial, Helvetica, sans-serif; font-size:12px; border-left:1px solid #dfdfdf\" width=\"20\" height=\"100%\" align=\"left\" valign=\"top\">&nbsp;</td>");					  
						sb.append("<td  style=\"font-family:Arial, Helvetica, sans-serif; font-size:12px;\" width=\"550\" align=\"left\" valign=\"top\" height=\"100%\">");
						sb.append("<p style=\"font-family:Arial, Helvetica, sans-serif; font-size:12px;margin:0 0 10px 0; padding:0\">");
						sb.append("<span style=\"width:135px\"><strong>Title : </strong></span>").append(assetTitle);
						sb.append("</p>");
						sb.append("<p style=\"font-family:Arial, Helvetica, sans-serif; font-size:12px;margin:0 0 10px 0; padding:0\">");
						sb.append("<span style=\"width:135px\"><strong>File Name : </strong></span>").append(assetcontent.getParent().getName());
						sb.append("</p>");
						sb.append("<p style=\"font-family:Arial, Helvetica, sans-serif; font-size:12px;margin:0 0 10px 0; padding:0\">");
						sb.append("<span style=\"width:135px\"><strong>Content Author : </strong></span>").append(contentAuthorName);
						sb.append("</p>");
						sb.append("<p style=\"font-family:Arial, Helvetica, sans-serif; font-size:12px;margin:0 0 10px 0; padding:0\">");
						sb.append("<span style=\"width:135px\"><strong>Folder Path : </strong></span>").append(assetcontent.getParent().getParent().getPath());
						sb.append("</p>");
						sb.append("<p style=\"font-family:Arial, Helvetica, sans-serif; font-size:12px;margin:0 0 10px 0; padding:0\">");
						sb.append("<span style=\"width:135px\"><strong>Expiration Date : </strong></span>").append(expiryTime);
						sb.append("</p>");
						sb.append("<p style=\"font-family:Arial, Helvetica, sans-serif; font-size:12px;margin:0 0 5px 0; padding:0\"><img src=\"http://www.hds.com/assets/img/expirationimg-1.gif\" alt=\"\" width=\"484\" height=\"1\" /></p>");
						sb.append("<p style=\"font-family:Arial, Helvetica, sans-serif; font-size:12px;margin:0 0 5px 0; padding:0\">");

						if(hostAddress!=null && StringUtils.isNotEmpty(hostAddress)){
						sb.append("<a href=\""+hostAddress+assetcontent.getParent().getPath()+"\" style=\"color:#CC0000\">Extend by one year</a> &nbsp;&nbsp;");
						}

						/*sb.append("<a href=\"#\" style=\"color:#CC0000\">Expire from AMP</a>&nbsp;&nbsp;");
						sb.append("<a href=\"#\" style=\"color:#CC0000\"> Expire from AMP and hds.com</a>");*/
						sb.append("</p>");
						sb.append("<p style=\"font-family:Arial, Helvetica, sans-serif; font-size:12px;margin:0 0 5px 0; padding:0\"><img src=\"http://www.hds.com/assets/img/expirationimg-1.gif\" alt=\"\" width=\"484\" height=\"1\" /></p>");
						sb.append("</td>");
						sb.append("<td  style=\"font-family:Arial, Helvetica, sans-serif; font-size:12px; border-right:1px solid #dfdfdf\" width=\"20\" height=\"100%\" align=\"right\" valign=\"top\">&nbsp;</td>");
						sb.append("</tr>");	
						
						Map<String, String> asstDetailsMap= new HashMap<String,String>();
						//asstDetailsMap.put("emailIds", emailIds);
						asstDetailsMap.put(assetcontent.getParent().getPath(), sb.toString());
						
						if(!assetAndEmail.containsKey(ownerMailId)){											
							assetAndEmail.put(ownerMailId, asstDetailsMap);
						}else if(assetAndEmail.containsKey(ownerMailId)){
							Map<String,String> getAsstDetails = assetAndEmail.get(ownerMailId);
							getAsstDetails.put(assetcontent.getParent().getPath(), sb.toString());
						}
						
	
					}
				}
				resultCount++;
			}
			LOG.info("Search Result Count : " + resultCount);
			//if(sb.toString().length() > 0){
			if(assetAndEmail.size() > 0){
				for(Entry<String, Map<String, String>> asset:assetAndEmail.entrySet()){
					List<InternetAddress> toMailId = new ArrayList<InternetAddress>();
					toMailId.add(new InternetAddress(asset.getKey().toString()));
					StringBuilder asb = new StringBuilder();
					if(asset.getValue().size() > 0){
						for(Entry<String, String> assetDet:asset.getValue().entrySet()){
							asb.append(assetDet.getValue());
						}
					}
					sendAssetNotificationEmail(messageGatewayService, messageGateway, repository, emailRecipients,
							asb.toString(), damPath, notifyType, emailTemplatePath,toMailId, resourceResolver);
				
				}
				
			}
			
		} catch (LoginException e) {
			LOG.error("Login Exception : ",e);
		} catch (RepositoryException e) {
			LOG.error("Repository Exception : ",e);
		} catch (AddressException e) {
			LOG.error("Email Id Error : " ,e);
		}finally{
			if(session!=null){
				session.logout();
			}
		}
		//return sb.toString();
	}

	public static void sendAssetNotificationEmail(MessageGatewayService messageGatewayService, MessageGateway<HtmlEmail> messageGateway,
			SlingRepository repository, List<InternetAddress> emailRecipients, String assetList, 
			String damPath, String notifyType, String template, List<InternetAddress> toMailId, ResourceResolver resourceResolver) {
		LOG.info("Entry into sendEmergencyEmail method");
		//ArrayList<InternetAddress> emailRecipients = new ArrayList<InternetAddress>();
		Map<String, String> map = new HashMap<String, String>();
		String language = "en";

		try{
			//emailRecipients.add(new InternetAddress(userMailId));
			//emailRecipients.add(new InternetAddress("brittoraj1988@gmail.com"));
			LOG.info("Email Ids : " + emailRecipients.toString());
			HtmlEmail email = new HtmlEmail();
			//String template = "/etc/workflow/notification/email/assetexpiry/en.txt";                          
			String defaultWorkspace = repository.getDefaultWorkspace();
			Session session = repository.loginAdministrative(defaultWorkspace);

			/*Configuration config = (Configuration) configAdmin.getConfiguration("com.day.cq.workflow.impl.email.EMailNotificationService");
			Dictionary props = config.getProperties();
			String hostPrefix = (String) props.get("host.prefix");

			map.put("host.prefix", hostPrefix);*/

			//map.put("recipientName", userMailId);
			LOG.info("Asset List : " +assetList);
			map.put("bodycontent", assetList);
			if(notifyType.equals("1")){
				map.put("days", notifyType+" day");
			}else{
				map.put("days", notifyType+" days");
			}
			DateFormat df = new SimpleDateFormat("yy"); // Just the year, with 2 digits
			String formattedDate = df.format(Calendar.getInstance().getTime());		
			map.put("year", formattedDate);
			
			final MailTemplate mailTemplate = MailTemplate.create(template+"/expiry-notification.html", session);			
			StrLookup strLookup = StrLookup.mapLookup(map);
			LOG.debug("******Befor Mail Template **********************");
			email = mailTemplate.getEmail(strLookup, HtmlEmail.class);
			//LOG.info(email.getMimeMessage().getContent().toString());
			LOG.debug("******Set the recipents in the email **********************");
			if(toMailId.size() > 0){
			email.setTo(toMailId);
			}else{
				if(emailRecipients.size() > 0){
					email.setTo(emailRecipients);
				}
			}
			if(emailRecipients.size() > 0){
				email.setCc(emailRecipients);
			}
			email.setSubject(notifyType+"Days Email Notification for "+damPath);

			messageGateway = messageGatewayService.getGateway(HtmlEmail.class);
			messageGateway.send(email);
			LOG.debug("Sent the Email **********************");
		}
		catch ( Exception e ) {
			LOG.error( "Fatal error while sending email in sendAssetNotificationEmail", e );
		}
		LOG.info("Exit from sendEmergencyEmail method");
	}
}
