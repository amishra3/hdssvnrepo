package com.hdscorp.cms.slingmodels;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.Property;
import javax.jcr.PropertyType;
import javax.jcr.RepositoryException;
import javax.jcr.Value;
import javax.jcr.ValueFormatException;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceUtil;
import org.apache.sling.api.resource.ValueMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.hdscorp.cms.config.HdsCorpGlobalConfiguration;
import com.hdscorp.cms.constants.GlobalConstants;
import com.hdscorp.cms.dao.ResourceNode;
import com.hdscorp.cms.util.PathResolver;
import com.hdscorp.cms.util.ServiceUtil;

public class ResourceLibraryHelperModel {
	
	private static final Logger LOG = LoggerFactory.getLogger(ResourceLibraryHelperModel.class);
	
	public static ResourceNode getResourceNode(Resource resource,
			String[] contenttype, String[] industryTag, TagManager tagManager,SlingHttpServletRequest request) throws ValueFormatException, PathNotFoundException, RepositoryException {

		String videoSortDateProperty = (String)HdsCorpGlobalConfiguration.getPropertyValue(HdsCorpGlobalConfiguration.RL_SORT_VIDEO_PROP);
		String externalContentSortDateProperty = (String)HdsCorpGlobalConfiguration.getPropertyValue(HdsCorpGlobalConfiguration.RL_SORT_EXTERNAL_PROP);
		String pdfSortDateProperty = (String)HdsCorpGlobalConfiguration.getPropertyValue(HdsCorpGlobalConfiguration.RL_SORT_PDF_PROP);
		
		Resource metadataResource = resource.getChild("jcr:content/metadata");
		Node metadataNode = metadataResource.adaptTo(Node.class) ; 
		
		if (metadataResource != null) {

			ResourceNode resourceNode = new ResourceNode();
			ValueMap properties = ResourceUtil.getValueMap(metadataResource);

			if (properties.containsKey("dc:title")) {
				resourceNode.setResourceTitle(properties.get("dc:title").toString());
			} else if (properties.containsKey("dc:hdstitle")) {
				resourceNode.setResourceTitle(properties.get("dc:hdstitle").toString());
			}
			if (properties.containsKey("dc:description")) {
				resourceNode.setResourceDescription(properties.get("dc:description").toString());
			}
			
			if (properties.containsKey("contentpath")) {
				resourceNode.setExternalContentTargetURL(properties.get("contentpath").toString());
			}
			
			LOG.info("resource Path::"+resource.getPath());
			
			if(resource.getPath().contains("en_us") || resource.getPath().contains("en-us")){
				resourceNode.setResourceLocale("en_us");
			}else{
				resourceNode.setResourceLocale("");
			}
			resourceNode.setResourcePath(PathResolver.getShortURLPath(resource.getPath()));
			if (properties.containsKey("cq:tags")) {
				String[] assetTags = (String[]) properties.get("cq:tags");
                boolean hasContentType = false;
				
				List<String> industryTadIds = new ArrayList<>();
				resourceNode.setResourceTags(assetTags);
				int i = 0;
				for (String item : assetTags) {
					Tag tag = tagManager.resolve(item);
					if (tag != null) {
						if (contenttype.length > 0 && item.contains(contenttype[0])) {
							
							resourceNode.setContentType(tag.getTitle());
							resourceNode.setContentTypeTag(tag.getTagID());
							hasContentType = true;
						}
						if (industryTag.length > 0 && item.contains(industryTag[0])) {
							industryTadIds.add(tag.getTagID());
						}
					}
					
					if(!hasContentType && resourceNode.getExternalContentTargetURL()== null){
						resourceNode.setContentType("Video");
					}

					if(industryTadIds.size()>0){
						resourceNode.setIndustryTags(industryTadIds.toArray(new String[0]));
					} else {
						resourceNode.setIndustryTags(new String[] {""});
					}
				}

				
			}
			
			try {
				if (properties.containsKey("resourceType")) {
					resourceNode.setResourceType(properties.get("resourceType").toString());
					if(resourceNode.getResourceType().equalsIgnoreCase("video")) {
//						if(StringUtils.isEmpty(resourceNode.getContentType())){
							resourceNode.setContentType(GlobalConstants.RESOURCE_TYPE_VIDEO);	
//						}
						resourceNode.setVideoGuid(properties.get("guid").toString());
						resourceNode.setVideoTitleId(properties.get("titleId").toString());	
						if(properties.containsKey(videoSortDateProperty)){
							Calendar pubDateCalObj =  getDateObj(metadataNode, videoSortDateProperty) ;
							long videoPubDateInMilliSec = pubDateCalObj.getTimeInMillis();
							resourceNode.setComparisonDateInMilliSec(videoPubDateInMilliSec);
							resourceNode.setComparisonDate(pubDateCalObj.getTime());
						}
					}
				}else {
					if(resourceNode.getExternalContentTargetURL()!= null){
						resourceNode.setResourceType(GlobalConstants.RESOURCE_TYPE_EXTERNAL_CONTENT);
						if(metadataNode!=null && metadataNode.hasProperty(externalContentSortDateProperty))
						{
							Calendar lastModDate = getDateObj(metadataNode, externalContentSortDateProperty) ;
							long lastModInMilliSec = lastModDate.getTimeInMillis();
							resourceNode.setComparisonDateInMilliSec(lastModInMilliSec);
							resourceNode.setComparisonDate(lastModDate.getTime());
						}					
					}else{
						resourceNode.setResourceType(GlobalConstants.RESOURCE_TYPE_PDF);
						if(metadataNode!=null && metadataNode.hasProperty(pdfSortDateProperty)){
							Calendar pdfModifiedDate = getDateObj(metadataNode, pdfSortDateProperty) ;
							long pdfModifiedDateInMilliSec = pdfModifiedDate.getTimeInMillis();
							resourceNode.setComparisonDateInMilliSec(pdfModifiedDateInMilliSec);
							resourceNode.setComparisonDate(pdfModifiedDate.getTime());
						}
					}
				}
			} catch (Exception e) {
				LOG.error("Exception in ResourceLibraryHelperModel ---- "+e.getMessage());
			}
			
			return resourceNode;
		}
		return null;
	}
	
	private static Calendar getDateObj(Node metadataNode, String datePropNameString) {
		Calendar returnDate = null;
		
		try {
			Value dateProp = metadataNode.getProperty(datePropNameString).getValue() ;
			if(dateProp!=null && dateProp.getType()== PropertyType.DATE){
				returnDate = dateProp.getDate();	
			}else if (dateProp!=null && dateProp.getType()== PropertyType.STRING){
				Date pubDate = ServiceUtil.getDateFromString(dateProp.toString(),"EEE, d MMM yyyy HH:mm:ss Z");
				returnDate =  Calendar.getInstance();
				returnDate.setTime(pubDate);
			}
			
		} catch (ValueFormatException e) {
			LOG.error("Exception in ValueFormatException ---- "+e.getMessage());
		} catch (PathNotFoundException e) {
			LOG.error("Exception in PathNotFoundException ---- "+e.getMessage());
		} catch (RepositoryException e) {
			LOG.error("Exception in RepositoryException ---- "+e.getMessage());
		} catch (ParseException e) {
			LOG.error("Exception in ParseException ---- "+e.getMessage());
		}
		return returnDate ;
	}
}
