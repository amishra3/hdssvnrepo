package com.hdscorp.cms.slingmodels;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.Page;
import com.hdscorp.cms.dao.ResourceNode;
import com.hdscorp.cms.search.SearchServiceHelper;
import com.hdscorp.cms.util.HdsCorpCommonUtils;
import com.hdscorp.cms.util.JcrUtilService;
import com.hdscorp.cms.util.ViewHelperUtil;

@Model(adaptables = { SlingHttpServletRequest.class, Resource.class })
public class ResourceLibrarySearchModel {

	@Inject
	private SlingHttpServletRequest request;

	private List<ResourceNode> resouceList;

	@Inject
	private Page resourcePage;

	private int totalNoOfResuts;
	@Inject
	@Named("contenttype")
	@Default(values = { "" })
	private String[] contenttype;
	@Inject
	@Named("industrytag")
	@Default(values = { "" })
	private String[] industrytag;

	private String defaultResourcePath[] = { "/content/dam/public/en_us/pdfs", "/content/dam/public/en_us/videos",
			"/content/dam/public/en_us/ext" };

	private String[] selectorTags;

	private static final Logger LOG = LoggerFactory.getLogger(ResourceLibrarySearchModel.class);

	public String[] getSelectorTags() {
		return selectorTags;
	}

	private boolean noTags;

	public boolean isNoTags() {
		String[] selectorArray = request.getRequestPathInfo().getSelectors();
		if (selectorArray == null || selectorArray.length == 0) {

			return true;
		} else {
			String viewtype = selectorArray[0];
			viewtype = viewtype.replaceAll("\\^", "/").replaceAll("[\\[\\](){}]", "").replaceAll("~", ":");
			this.selectorTags = viewtype.split(",");

			return false;
		}

	}

	public List<ResourceNode> getResouceList() throws Exception {

		TagManager tagManager = JcrUtilService.getResourceResolver().adaptTo(TagManager.class);
		LOG.info("-------------INSIDE Resouce Library  SEARCH Model");

		ResourceResolver resourceResolver = JcrUtilService.getResourceResolver();
		SearchServiceHelper searchServiceHelper = (SearchServiceHelper) ViewHelperUtil
				.getService(com.hdscorp.cms.search.SearchServiceHelper.class);
		String fullText = request.getParameter("fulltext");
		try {
			if (fullText != null) {
				fullText = URLDecoder.decode(request.getParameter("fulltext"), "UTF-8");
			}

		} catch (Exception e) {
			LOG.info("Exception while decoding the url::" + e.getMessage());
		}

		String viewtype = "";
		String sortCriteria = "default";
		String[] selectorArray = request.getRequestPathInfo().getSelectors();
		String tags[] = { "" };

		if (selectorArray != null && selectorArray.length > 0) {
			if (selectorArray.length > 1) {
				sortCriteria = selectorArray[1];
			}

			if (selectorArray[0].contains("common")) {
				viewtype = selectorArray[0];
				viewtype = viewtype.replaceAll("\\^", "/").replaceAll("[\\[\\](){}]", "").replaceAll("~", ":");
				tags = viewtype.split(",");
			} else {
				sortCriteria = selectorArray[0];
				viewtype = null;
				tags = null;
			}

		} else {
			tags = null;
		}

		Resource parResource = resourcePage.getContentResource("par");
		Resource resourceMetaData = parResource.getChild("resourcelibrarysearc");
		ValueMap pageProperty = resourceMetaData.adaptTo(ValueMap.class);

		String pdfsPath[] = pageProperty.get("pdfspath", String[].class);

		String videospath[] = pageProperty.get("videospath", String[].class);

		String externalassetpath[] = pageProperty.get("externalassetpath", String[].class);

		LOG.info("videospath::" + videospath);
		LOG.info("pdfsPath::" + pdfsPath);
		LOG.info("externalassetpath::" + externalassetpath);

		List<String> allPath = new ArrayList<String>();
		if (pdfsPath != null) {
			checkEmptyArray(allPath, pdfsPath);
		}
		if (videospath != null) {
			checkEmptyArray(allPath, videospath);
		}
		if (externalassetpath != null) {
			checkEmptyArray(allPath, externalassetpath);
		}

		LOG.info("allPath size:::" + allPath.size());

		String paths[] = defaultResourcePath;

		String finalAllPath[] = new String[allPath.size()];

		if (allPath != null && allPath.size() > 0) {
			for (int index = 0; index < allPath.size(); index++) {
				finalAllPath[index] = allPath.get(index);
			}
		}
		if (allPath != null && allPath.size() > 0) {
			paths = finalAllPath;
		}

		boolean doPagination = false;
		String type[] = { "dam:Asset", "cq:Page" };

		SearchResult result = searchServiceHelper.getFullTextBasedResuts(paths, tags, null, type, fullText,
				doPagination, null, null, resourceResolver, null, null);
		List<Hit> hits = result.getHits();

		LOG.info("-------------Hits Size-----" + result.getHits().size());
		LOG.info("-------------Resource Library Query Excecution Time-----" + result.getExecutionTime());

		resouceList = new ArrayList<ResourceNode>();
		List<ResourceNode> localeresourceNodeList = new ArrayList<ResourceNode>();
		List<ResourceNode> enresourceNodeList = new ArrayList<ResourceNode>();
		this.totalNoOfResuts = hits.size();
		for (Hit hit : hits) {
			ResourceNode resourceNode = ResourceLibraryHelperModel.getResourceNode(hit.getResource(), this.contenttype,
					this.industrytag, tagManager, request);
			resourceNode.setGated(HdsCorpCommonUtils.isGated(resourceNode.getResourcePath(), request));
			if (resourceNode != null && resourceNode.getResourceLocale().equalsIgnoreCase("")) {
				localeresourceNodeList.add(resourceNode);
			} else if (resourceNode != null) {
				// resouceList.add(resourceNode);
				enresourceNodeList.add(resourceNode);
			}
		}

		LOG.info("GEO List Size::" + localeresourceNodeList.size());
		if ("default".equals(sortCriteria)) {
			Collections.sort(localeresourceNodeList, new GatedFirstComparator());
		} else if ("alpha".equals(sortCriteria)) {
			Collections.sort(localeresourceNodeList, new LexicographicComparator());
		} else if ("date".equals(sortCriteria)) {
			Collections.sort(localeresourceNodeList, new dateComparator());
		}

		if ("default".equals(sortCriteria)) {
			Collections.sort(enresourceNodeList, new GatedFirstComparator());
		} else if ("alpha".equals(sortCriteria)) {
			Collections.sort(enresourceNodeList, new LexicographicComparator());
		} else if ("date".equals(sortCriteria)) {
			Collections.sort(enresourceNodeList, new dateComparator());
		}

		resouceList.addAll(localeresourceNodeList);
		resouceList.addAll(enresourceNodeList);
		return resouceList;
	}

	public int getTotalNoOfResuts() {
		return totalNoOfResuts;
	}

	class LexicographicComparator implements Comparator<ResourceNode> {
		@Override
		public int compare(ResourceNode a, ResourceNode b) {
			return a.getResourceTitle().compareToIgnoreCase(b.getResourceTitle());
		}
	}

	class dateComparator implements Comparator<ResourceNode> {
		@Override
		public int compare(ResourceNode a, ResourceNode b) {
			return Long.compare(b.getComparisonDateInMilliSec(), a.getComparisonDateInMilliSec());
		}
	}

	class GatedFirstComparator implements Comparator<ResourceNode> {
		@Override
		public int compare(ResourceNode a, ResourceNode b) {
			int gatedCompareResult = Boolean.valueOf(b.isGated()).compareTo(Boolean.valueOf(a.isGated()));
			// if(gatedCompareResult==0){
			// gatedCompareResult =
			// a.getResourceTitle().compareTo(b.getResourceTitle());
			// }
			return gatedCompareResult;
		}
	}

	public void checkEmptyArray(List<String> list, String array[]) {
		for (int i = 0; i < array.length; i++) {
			if (!array[i].trim().isEmpty())
				list.add(array[i]);
		}
	}
}
