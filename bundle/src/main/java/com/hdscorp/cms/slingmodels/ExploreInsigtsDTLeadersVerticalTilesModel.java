package com.hdscorp.cms.slingmodels;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
@Model(adaptables = Resource.class)
public class ExploreInsigtsDTLeadersVerticalTilesModel {
	@Inject
	@Named("eidtbgimagepath")
	@Default(values = { "" })
	private String eidtBGImagePath;
	@Inject
	@Named("eidtblogtilte")
	@Default(values = { "" })
	private String eidtBlogTilte;

	@Inject
	@Named("eidtblogdescription")
	@Default(values = { "" })
	private String eidtBlogDescription;

	@Inject
	@Named("eidtlabeltargeturl")
	@Default(values = { "" })
	private String eidtLabelTargetURL;

	@Inject
	@Named("eidticonbimagelabel")
	@Default(values = { "" })
	private String eidtIconBImageLabel;

	@Inject
	@Named("eidticonbimagepath")
	@Default(values = { "" })
	private String eidtIconBImagePath;

	@Inject
	@Named("eidtbblogdescription")
	@Default(values = { "" })
	private String eidtBBlogBescription;
	
	@Inject
	@Named("eidtlabelbtargeturl")
	@Default(values = { "" })
	private String eidtLabelBTargetURL;

	@Inject
	@Named("eidticonreadmorelabel")
	@Default(values = { "" })
	private String eidtIconReadmoreLabel;

	@Inject
	@Named("eidttopeninnewwindow")
	@Default(values = { "" })
	private String eidttOpenInNewwindow;

	@Inject
	@Named("eidttbopeninnewwindow")
	@Default(values = { "" })
	private String eidttOpenBInNewwindow;

	public String getEidtBGImagePath() {
		return eidtBGImagePath;
	}

	public String getEidtBlogTilte() {
		return eidtBlogTilte;
	}

	public String getEidtBlogDescription() {
		return eidtBlogDescription;
	}

	public String getEidtLabelTargetURL() {
		return eidtLabelTargetURL;
	}

	public String getEidtIconBImageLabel() {
		return eidtIconBImageLabel;
	}

	public String getEidtIconBImagePath() {
		return eidtIconBImagePath;
	}

	public String getEidtBBlogBescription() {
		return eidtBBlogBescription;
	}

	public String getEidtLabelBTargetURL() {
		return eidtLabelBTargetURL;
	}

	public String getEidtIconReadmoreLabel() {
		return eidtIconReadmoreLabel;
	}

	public String getEidttOpenInNewwindow() {
		return eidttOpenInNewwindow;
	}

	public String getEidttOpenBInNewwindow() {
		return eidttOpenBInNewwindow;
	}

}
