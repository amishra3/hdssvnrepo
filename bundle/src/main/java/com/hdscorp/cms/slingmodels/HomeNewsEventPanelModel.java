package com.hdscorp.cms.slingmodels;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;

/**
 * Sling model for Home News and Event Panel component
 * 
 * @author gokula.nand
 *
 */
@Model(adaptables = Resource.class)
public class HomeNewsEventPanelModel {

	@Inject
	@Named("jcr:henpheadinglabel")
	@Default(values = { "" })
	private String headingLabel;

	@Inject
	@Named("jcr:henpimagepath")
	@Default(values = { "" })
	private String imagePath;

	@Inject
	@Named("jcr:henpdate")
	@Default(values = { "" })
	private String itemdate;

	@Inject
	@Named("jcr:henptitle")
	@Default(values = { "" })
	private String title;

	@Inject
	@Named("jcr:henpdescription")
	@Default(values = { "" })
	private String description;

	@Inject
	@Named("jcr:henpctalabel")
	@Default(values = { "" })
	private String ctaLabel;

	@Inject
	@Named("jcr:henpctalink")
	@Default(values = { "" })
	private String ctaLink;

	public String getHeadingLabel() {
		return headingLabel;
	}

	public String getImagePath() {
		return imagePath;
	}

	public String getItemdate() {
		return itemdate;
	}

	public String getTitle() {
		return title;
	}

	public String getDescription() {
		return description;
	}

	public String getCtaLabel() {
		return ctaLabel;
	}

	public String getCtaLink() {
		return ctaLink;
	}
	

}
