package com.hdscorp.cms.slingmodels;

import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.dam.api.Asset;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.hdscorp.cms.dao.PDFNode;
import com.hdscorp.cms.util.HdsCorpCommonUtils;
import com.hdscorp.cms.util.PathResolver;
import com.hdscorp.cms.util.ServiceUtil;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;

/**
 * This sling model is used for get all meta data of PDF nodes.
 * 
 * @author gokula.nand
 *
 */
@Model(adaptables = { SlingHttpServletRequest.class, Resource.class })
public class PDFMetaModel {

	private static final Logger log = LoggerFactory.getLogger(PDFMetaModel.class);

	private final String FROM_DATE = "yyyy-MM-dd'T'HH:mm:ss.SSSX";
	private final String TO_DATE = "MMMM yyyy";

	@Inject
	private SlingHttpServletRequest request;

	@Inject
	private ResourceResolver resourceResolver;

	private PDFNode pdfNode;

	public PDFNode getPdfNode() {
		String pdfPath = request.getParameter("pdfPath");
		if (pdfPath == null) {
			pdfPath = (String) request.getAttribute("pdfPath");
		}

		if (pdfPath == null) {
			pdfPath = request.getRequestURI();
		}

		log.info("Start Execution of getPdfNode() PdfPath::" + pdfPath);
		try {
			if (pdfPath != null && !pdfPath.isEmpty()
					&& (pdfPath.toLowerCase().contains(".pdf") || pdfPath.contains("/ext/"))) {
				if (!pdfPath.startsWith("/content/")) {
					pdfPath = HdsCorpCommonUtils.pdfJCRPath(pdfPath);
				}

				pdfNode = new PDFNode();
				Resource resource = resourceResolver.resolve(pdfPath);

				if (resource == null || resource.getResourceType().equals("sling:nonexisting")) {
					resource = PathResolver.getResourceFromShortURL(request, pdfPath);
				}

				if (!resource.isResourceType(Resource.RESOURCE_TYPE_NON_EXISTING)) {

					Asset asset = resource.adaptTo(Asset.class);

					TagManager tagManager = resourceResolver.adaptTo(TagManager.class);

					if (asset != null) {
						pdfNode.setTitle(asset.getMetadataValue("dc:title") != null
								? asset.getMetadataValue("dc:title").toString() : "");
						pdfNode.setDescription(asset.getMetadataValue("pdf:summary") != null
								? asset.getMetadataValue("pdf:summary").toString() : "");
						pdfNode.setImagePath(asset.getMetadataValue("dc:imagePath") != null
								? asset.getMetadataValue("dc:imagePath").toString() : "");
						if (asset.getMetadataValue("dc:creationdate") != null
								&& !asset.getMetadataValue("dc:creationdate").trim().isEmpty()) {
							pdfNode.setCreatedDate(ServiceUtil.getDisplayDateFormat(
									asset.getMetadataValue("dc:creationdate").toString(), FROM_DATE, TO_DATE));
						} else {
							pdfNode.setCreatedDate("");
						}
						if (asset.getMetadataValue("pdfx:prop1") != null && !asset.getMetadataValue("pdfx:prop1").trim().equalsIgnoreCase("")) {
							pdfNode.setSubTitle(asset.getMetadataValue("pdfx:prop1"));
						} else {
							pdfNode.setSubTitle(asset.getMetadataValue("jcr:title") != null
									? asset.getMetadataValue("jcr:title").toString() : "");
						}

						if (asset.getMetadataValue("cq:tags") != null) {
							Object[] tags = (Object[]) asset.getMetadata("cq:tags");
							for (Object obj : tags) {
								Tag tag = tagManager.resolve(obj.toString());

								if (tag.getTagID().toString().contains("common:content-type")) {
									log.info("tag id::" + tag.getTagID());
									pdfNode.setContentType(tag.getTitle());
								}

							}
						}
					} else {
						Resource metaDataResource = resource.getChild("jcr:content/metadata");
						ValueMap resVMap = metaDataResource.adaptTo(ValueMap.class);
						
						pdfNode.setTitle(resVMap.get("dc:title", ""));
						pdfNode.setDescription(resVMap.get("dc:description", ""));
						pdfNode.setLongDescription(resVMap.get("dc:longdescription", ""));
						pdfNode.setCreatedDate(ServiceUtil.getDisplayDateFormat(
								resVMap.get("dc:creationdate", "").toString(), FROM_DATE, TO_DATE));
						pdfNode.setExternalContentURL(resVMap.get("contentpath", ""));
						pdfNode.setCtaLinkoption(resVMap.get("overcta", ""));
						pdfNode.setCtaLinktitle(resVMap.get("ctatitle", ""));
						pdfNode.setCtaLincontent(resVMap.get("ctacontent", ""));

						if (resVMap.get("pdfx:prop1") != null && !resVMap.get("pdfx:prop1","").trim().equalsIgnoreCase("")) {
							pdfNode.setSubTitle(resVMap.get("pdfx:prop1", "").toString());
						} else {
							pdfNode.setSubTitle(
									resVMap.get(resVMap.get("jcr:title", "") != null ? resVMap.get("jcr:title", "") : "")
											.toString());
						}

						if (resVMap.get("cq:tags") != null) {
							String tags[] = (String[]) resVMap.get("cq:tags", String[].class);
							for (String str : tags) {
								Tag tag = tagManager.resolve(str.toString());
								if (tag.getTagID().toString().contains("common:content-type")) {
									log.info("tag id::" + tag.getTagID());
									pdfNode.setContentType(tag.getTitle());
								}

							}
						}
						if (resVMap.get("cq:tags") != null) {
							String tags[] = (String[]) resVMap.get("cq:tags",String[].class);
							for (String str : tags) {
								Tag tag = tagManager.resolve(str.toString());
								if (tag.getTagID().toString().contains("common:content-type")) {
									log.info("tag id::" + tag.getTagID());
									pdfNode.setContentType(tag.getTitle());
								} 

							}
						}
					}
				}

			}
		} catch (Exception e) {
			log.error("Error occurs while gettting data from PDF meta Data::" + e.getMessage());
		}
		return pdfNode;
	}

}
