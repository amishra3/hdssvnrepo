package com.hdscorp.cms.slingmodels;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.TimeZone;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.jcr.RepositoryException;

import org.apache.sling.api.resource.ModifiableValueMap;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.runmode.RunMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.ListIterator;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.day.cq.wcm.api.Page;
import com.hdscorp.cms.util.PathResolver;
import com.hdscorp.cms.util.ServiceUtil;

@Model(adaptables = Resource.class)
public class PressReleaseDetailModel {
	
	private static final Logger LOG = LoggerFactory.getLogger(PressReleaseDetailModel.class);
	
	@Inject
    private Page resourcePage;
	
	@Inject
    private Resource resource;
		
	@Inject
	RunMode runmode;
	
	@Inject
	@Named("pressreleasetitle")
	private String pressReleaseTitle;
	
	@Inject
	@Named("pressreleasedate")
	private String pressReleaseDate;
	
	@Inject
	@Named("pressreleasedesc")
	private String pressReleaseDescription;
	
	@Inject
	@Named("timezone")
	@Default(values = { "" })
	private String timeZone;
	
	
	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}


	private String viewAllPRLabel;
	private String viewAllPRLink;
	public String getViewAllPRLabel() {

	return getProperty("viewallprlabel");
	}

	public String getViewAllPRLink() {
		return PathResolver.getShortURLPath(getProperty("viewallprlink"));
	}

	

	public String getPressReleaseTitle() {
		return pressReleaseTitle;
	}

	public String getPressReleaseDate() {
		return pressReleaseDate;
	}

	public String getPressReleaseDescription() {
		
		try {
			Document document = Jsoup.parse(pressReleaseDescription);
			Elements links = document.select("a[href]");
			if (links != null) {
				ListIterator<Element> elementsListItr = links.listIterator();
				while (elementsListItr.hasNext()) {
					Element element = elementsListItr.next();
					String oldhref = element.attr("href");
	                    				if(oldhref.startsWith("mailto:"))
	                    				{
	                    					oldhref = oldhref.replaceAll("@", "(at)");
	                    					oldhref = oldhref.replaceAll("mailto:","");
	                    					element.attr("href", "javascript:void(0);");
		                    				element.attr("lang", oldhref);
		                    				element.attr("rel", "emailHome");
	                    				}
	                    				
								}
							}
			pressReleaseDescription = document.select("body").html();
			} catch (Exception e){

			LOG.error("Exception while fixing the press release Description - "+e.getMessage());
		}
		
		return pressReleaseDescription;
	}

	
	private String getProperty(String property){
		String value = "";
		Page page = resourcePage.getAbsoluteParent(4);
		if(page!=null) {
			
		ValueMap properties =  page.getProperties();
		if(properties.containsKey(property)){
		value = properties.get(property).toString();
		}
		}
		
		return value;
	}
	
	@PostConstruct
    protected void fixTimeZone() throws RepositoryException {
		
		try {
			String [] runModes = runmode.getCurrentRunModes() ;
			
			if(Arrays.asList(runModes).contains("author")){
				Resource metaDataResource= null ;
				
				if(!resource.getPath().endsWith("pressrelease")){
					metaDataResource= resource.getChild("pressrelease");
				}else{
					metaDataResource= resource;
				}

				 if(metaDataResource!=null){
					 ModifiableValueMap  properties = metaDataResource.adaptTo(ModifiableValueMap.class);
					 SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
					 Calendar targetCal  =(Calendar) properties.get("pressreleasedate");
					 TimeZone targetTimezone = TimeZone.getTimeZone(timeZone);
					 Calendar updatedCal = ServiceUtil.updateTimezoneWithoutConversion(sdf,targetCal,targetTimezone);
					 try {
						 properties.put("pressreleasedate",updatedCal);
						 metaDataResource.getResourceResolver().commit();
					 } catch (Exception ex) {
						 LOG.error("Exception while fixing the press release timestamp - "+ex.getMessage());
					 }
				 }
			}

		} catch (Exception e) {
			LOG.error("Exception while fixing the press release timestamp - "+e.getMessage());
		}
	}

}
