package com.hdscorp.cms.scheduler;

import java.util.HashMap;
import java.util.Map;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.PropertyUnbounded;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hdscorp.cms.constants.ServiceConstants;
import com.hdscorp.cms.restservice.TwitterWebService;
import com.hdscorp.cms.util.HdsCorpCommonUtils;
import com.hdscorp.cms.util.ServiceUtil;

/**
 * {@link TwitterScheduler} Executes cron job, Stores TwitterFeed data in jcr
 * @author venkataramana
 *
 */
@Component(immediate = true, metatype = true, label = "TwitterScheduler ", description = "TwitterScheduler")
@Service(value = Runnable.class)
@Properties({
		@Property(name = ServiceConstants.TWITTER_ACCESS_DETAILS, description = "Provide Twitter Handle Detail for each import. For multiple, seperate them with '~~' character EX:- consumerkey=Q7yBVmniWA2TBWCgO8sl9SOXl~~consumerSecret=hvlVrHr8DbXYOtddhRL4kTUeqc4eborPDeJ4Im8ctiy2DV1wy1~~accessTokenKey=15354310-bevOoaxSL9TWCfzHAgHSTBwYxRfoFpvBmuSBgrKWY~~accessTokenSecret=NrnePhQ39PGprGZxW54s9SEAvbkm5cGoeY0Kk4OswHy5p~~storagePath=/content/hdscorp/en_us/lookup/twitterfeeddata/jcr:content/~~postlimit=3", value = "",unbounded = PropertyUnbounded.ARRAY),		
		@Property(name = ServiceConstants.TWITTER_SCHEDULER_EXPRESSION, description = "Default Cron Job", value = "0 * * * * ? ")})
public class TwitterScheduler implements Runnable{
	private static final Logger log = LoggerFactory.getLogger(TwitterScheduler.class);
	@Reference
	private TwitterWebService twitterService;
	private String schedulerExpression;
	private String[] twitterAccessDetails;
		
	/**
	 * Useful to running scheduler based on OSGI config properties.
	 * @param ctx
	 */
     @Activate
	protected void activate(ComponentContext ctx) {
		log.info("[TwitterScheduler]: Activated method called");
		this.schedulerExpression = ctx.getProperties().get(ServiceConstants.TWITTER_SCHEDULER_EXPRESSION).toString();
		HdsCorpCommonUtils commonUtils = new HdsCorpCommonUtils();
		this.twitterAccessDetails = commonUtils.getPropertyAsArray(ctx.getProperties().get(ServiceConstants.TWITTER_ACCESS_DETAILS));
	}
     
	@Override
	public void run() {
		try {
			if (twitterAccessDetails != null && twitterAccessDetails.length > 0) {
				for (String twitterAccessDetail : twitterAccessDetails) {
					Map<String, String> map = new HashMap<String, String>();
					String[] accountDeatails = twitterAccessDetail.split("~~");
					for (String keyValuePair : accountDeatails) {
						String[] keyValue = keyValuePair.split("=");
						if (keyValue != null && keyValue.length > 1) {
							map.put(keyValue[0], keyValue[1]);
						}
					}
					ServiceUtil.saveWSResponse(twitterService.getTwitterResponse(map.get(ServiceConstants.TWITTER_CONSUMER_KEY),
						map.get(ServiceConstants.TWITTER_CONSUMER_SECRET), map.get(ServiceConstants.TWITTER_ACCESSTOKEN_KEY),
						map.get(ServiceConstants.TWITTER_ACCESSTOKEN_SECRET), map.get(ServiceConstants.TW_POST_LIMIT)),
						map.get(ServiceConstants.TWITTER_STORAGE_PATH), ServiceConstants.TWITTER_SAVE_FEED_DATA_PROPERTY_NAME);
				}
			}
		} catch (Exception e) {
			log.error( "Exception occurs during cron job execution for twitter ", e);
		}
	}
}
