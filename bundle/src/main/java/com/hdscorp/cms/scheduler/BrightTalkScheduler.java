package com.hdscorp.cms.scheduler;

import java.util.HashMap;
import java.util.Map;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.PropertyUnbounded;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hdscorp.cms.config.HdsCorpGlobalConfiguration;
import com.hdscorp.cms.constants.ServiceConstants;
import com.hdscorp.cms.restservice.BrightTalkWebService;
import com.hdscorp.cms.util.HdsCorpCommonUtils;
import com.hdscorp.cms.util.ServiceUtil;

/**
 * This is a BrightTalk Scheduler service is used for getting feed posts from Brighttalk feed.
 * @author gokula.nand
 */
@Component(label = "BrightTalk Scheduler", description = "This service basically is used for consuming data from the feed", metatype = true, immediate = true)
@Service(value = Runnable.class)
@Properties({
	@Property(name = ServiceConstants.BRIGHTTALK_ACCESS_DETAILS, description = "Provide Brighttalk Details EX:- feed.url==https://www.brighttalk.com/channel/12821/feed~~storage.path==/content/hdscorp/en_us/test/jcr:content/", value = "", unbounded = PropertyUnbounded.ARRAY),
	@Property(name = ServiceConstants.FEED_SCHEDULER_EXPRESSION, description = "Default Cron Job", value = "0 25 13 * * ?")	})
public class BrightTalkScheduler implements Runnable{
	private static final Logger log = LoggerFactory.getLogger(BrightTalkScheduler.class);
	@Reference
	private BrightTalkWebService brightTalkService;
	private String schedulerExpression;
	private String[] brighttalkAccessDetails;
	
	/**
	 * Useful to running scheduler based on OSGI config properties.
	 * @param ctx
	 */
	@Activate
	protected void activate(ComponentContext ctx) {
		log.info("Execution start for activate method of BrightTalk Schedular");
		this.schedulerExpression = ctx.getProperties().get(ServiceConstants.FEED_SCHEDULER_EXPRESSION).toString();
		HdsCorpCommonUtils commonUtils = new HdsCorpCommonUtils();
		this.brighttalkAccessDetails = commonUtils.getPropertyAsArray(ctx.getProperties().get(ServiceConstants.BRIGHTTALK_ACCESS_DETAILS));
		log.info("Execution end for activate method of BrightTalk Schedular");
	}
	
	@Override
	public void run() {
		try {
			log.info("started brightTalk scheduler");
			if (null != brighttalkAccessDetails && brighttalkAccessDetails.length > 0) {
				for (String brighttalkAccessDetail : brighttalkAccessDetails) {
					Map<String, String> map = new HashMap<String, String>();
					String[] accountDetails = brighttalkAccessDetail.split("~~");
					for (String keyValuePair : accountDetails) {
						String[] keyValue = keyValuePair.split("==");
						if (null != keyValue && keyValue.length > 1) {
							map.put(keyValue[0], keyValue[1]);
						}
					}
					HdsCorpGlobalConfiguration.BRIGHTTALK_DATA_STORAGE_PATH = map.get(ServiceConstants.FEED_STORAGE_PATH);
					ServiceUtil.saveWSResponse(brightTalkService.getBrightTalkResponse(map.get(ServiceConstants.FEED_URL_KEY)), 
							map.get(ServiceConstants.FEED_STORAGE_PATH), ServiceConstants.SAVE_FEED_DATA_PROPERTY_NAME);
				}
			}
		} catch (Exception e) {
			log.error("Exception occurs duing cron job execution for brighttalk ", e);
		}
	}
}