package com.hdscorp.cms.scheduler;

import java.util.HashMap;
import java.util.Map;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.PropertyUnbounded;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hdscorp.cms.constants.ServiceConstants;
import com.hdscorp.cms.restservice.FacebookWebService;
import com.hdscorp.cms.util.HdsCorpCommonUtils;
import com.hdscorp.cms.util.ServiceUtil;

import facebook4j.FacebookException;

/**
 * This service is used for getting feed posts from Facebook based on OSGI
 * properties.
 * 
 * @author gokula.nand
 */

@Component(label = "Facebook Scheduler", description = "This service basically is used for consuming data from the facebook feed", metatype = true, immediate = true)
@Service(value = Runnable.class)
@Properties({
		@Property(name = ServiceConstants.FB_ACCESS_DETAILS, description = "Provide Facebook Handle Detail EX:- facebook.postlimit=3~~facebook.searchpost=9400567,facebook.appid=236001836745~~facebook.appsecret=6a29d7d82595a3884bd~~facebook.accesstoken=CAADW-pHWlQlsBAIiRt2qKzZAZAW3uyIwr5GMcPw0QFYrsZA9LAxxMqOikZAoRBPFa26bAzccYhjv9ZAHpyQa~~facebook.storage.path=/content/hdscorp/en_us/lookup/facebookpostdata/jcr:content/", value = "", unbounded = PropertyUnbounded.ARRAY),
		@Property(name = ServiceConstants.FB_POST_SCHEDULER_EXPRESSION, description = "Facebook Cron Job", value = "0 30 13 * * ?", label = "Cron expression defining when this Scheduled Service will run") })
public class FacebookSheduler implements Runnable {
	private static final Logger log = LoggerFactory.getLogger(FacebookSheduler.class);
	@Reference
	private FacebookWebService facebookWebService;
	private String schedulerExpression;
	private String[] facebookAccessDetails;

	/**
	 * Useful to running scheduler based on OSGI config properties.
	 * @param ctx
	 */
	@Activate
	protected void activate(ComponentContext ctx) {
		log.info("Execution start for activate method of Facebook Schedular");
		this.schedulerExpression = ctx.getProperties().get(ServiceConstants.FB_POST_SCHEDULER_EXPRESSION).toString();
		HdsCorpCommonUtils commonUtils = new HdsCorpCommonUtils();
		this.facebookAccessDetails = commonUtils.getPropertyAsArray(ctx.getProperties().get(ServiceConstants.FB_ACCESS_DETAILS));
	}
	
	@Override
	public void run() {
		try {
			log.info("started facebook import service");
			if (null != facebookAccessDetails && facebookAccessDetails.length > 0) {
				for(String facebookAccessDetail:facebookAccessDetails) {
					Map<String, String> map = new HashMap<String, String>();
					String[] accountDetails = facebookAccessDetail.split("~~");
					for (String keyValuePair : accountDetails) {
						String[] keyValue = keyValuePair.split("=");
						if (null != keyValue && keyValue.length > 1) {
							map.put(keyValue[0], keyValue[1]);
						}
					}
					ServiceUtil.saveWSResponse(facebookWebService.getFacebookFeed(map.get(ServiceConstants.FB_POST_SEARCH_KEY),
						map.get(ServiceConstants.FB_POST_LIMIT_KEY), map.get(ServiceConstants.FB_POST_APP_ID_KEY),
						map.get(ServiceConstants.FB_POST_APP_SECRET_KEY), map.get(ServiceConstants.FB_POST_APP_ACCESS_TOKEN_KEY)),
						map.get(ServiceConstants.FB_POST_STORAGE_PATH), ServiceConstants.SAVE_FB_FEED_DATA_PROPERTY_NAME);
				}
			}
		} catch (FacebookException e) {
			log.error( "Exception occurs duing cron job execution for facebook ", e);
		}
	}

}
