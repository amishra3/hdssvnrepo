package com.hdscorp.cms.scheduler;

import java.util.HashMap;
import java.util.Map;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.PropertyUnbounded;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hdscorp.cms.constants.ServiceConstants;
import com.hdscorp.cms.restservice.BrightCoveImporterService;
import com.hdscorp.cms.util.HdsCorpCommonUtils;

/**
 * This is a BrightCove Scheduler service is used for getting feed posts from Bright cove feed.
 * @author gokula.nand
 */
@Component(label = "BrightCove Scheduler", description = "This service basically is used for consuming data from the feed", metatype = true, immediate = true)
@Service(value = Runnable.class)
@Properties({
	@Property(name = ServiceConstants.BRIGHTCOVE_ACCESS_DETAILS, description = "Provide Brightcove Details EX:- feed.url==http://api.brightcove.com/services/library?command=search_videos&any=tag:hdscorp&output=mrss&token=J-KzSklqGjvSZ83MDVgB1Z3dYwbchmoH_8O2TX0j_JZflnvN9eqcNQ..~~brightcove.pagesize==100~~storage.path==/content/dam/public/en_us/videos", value = "", unbounded = PropertyUnbounded.ARRAY),
	@Property(name = ServiceConstants.FEED_SCHEDULER_EXPRESSION, description = "Default Cron Job", value = "0 15 10 * * ? 2005") })

public class BrightCoveScheduler implements Runnable {
	private static final Logger log = LoggerFactory.getLogger(BrightCoveScheduler.class);
	@Reference
	private BrightCoveImporterService brightCoveImporterService;
	private String[] brightcoveAccessDetails;
	private String schedulerExpression;

	/**
	 * Useful to running scheduler based on OSGI config properties.
	 * @param ctx
	 */
	@Activate
	protected void activate(ComponentContext ctx) {
		log.info("Execution start for activate method of Brightcove Schedular");
		this.schedulerExpression = ctx.getProperties().get(ServiceConstants.FEED_SCHEDULER_EXPRESSION).toString();
		HdsCorpCommonUtils commonUtils = new HdsCorpCommonUtils();
		this.brightcoveAccessDetails = commonUtils.getPropertyAsArray(ctx.getProperties().get(ServiceConstants.BRIGHTCOVE_ACCESS_DETAILS));
	}

	@Override
	public void run() {
		try {
			log.info("Started brightcove scheduler");
			if (null != brightcoveAccessDetails && brightcoveAccessDetails.length > 0) {
				for(String brightcoveAccessDetail : brightcoveAccessDetails) {
					Map<String, String> map = new HashMap<String, String>();
					String[] accountDetails = brightcoveAccessDetail.split("~~");
					for (String keyValuePair : accountDetails) {
						String[] keyValue = keyValuePair.split("==");
						if (null != keyValue && keyValue.length > 1) {
							map.put(keyValue[0], keyValue[1]);
						}
					}
					
					brightCoveImporterService.getBrightCoveResponse(map.get(ServiceConstants.FEED_URL_KEY),
							map.get(ServiceConstants.FEED_STORAGE_PATH),false,0,Integer.parseInt(map.get(ServiceConstants.BRIGHTCOVE_PAGE_SIZE)));
					
				}
			}
		} catch (Exception e) {
			log.error( "Exception occurs duing cron job execution for brightcove ", e);
		}
	}
	
}