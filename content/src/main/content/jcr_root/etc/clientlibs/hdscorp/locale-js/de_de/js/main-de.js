/* Country Specific JS Functions */
$(document).ready(function(){
	/* START - JS to check English site links */
	$("a").each(function(i){
		if($(this).attr('href').match("en-us") && $(this).attr('href').charAt(0) == '/'){
			if($(this).find('img').length > 0 || !$(this).text().trim().length){

			}else if($(this).find('span.glyphicon').length > 0){
				$(this).find('span.glyphicon').before(' <i>(en)</i>');
			}else{
				$(this).append(" <i>(en)</i>");
			}
		}
	});
	/* END - JS to check English site links */
})
