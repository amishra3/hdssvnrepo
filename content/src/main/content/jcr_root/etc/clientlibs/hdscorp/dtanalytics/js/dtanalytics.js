var digitalData = digitalData || {};
pageTitle=pageTitle.toLowerCase();
pageTitle=$.trim(pageTitle);
/*Get Country and language*/
var pathLoc= window.location.pathname;
var n= pathLoc.split("/");
var countLoc="";
var getCountry="";
var getLanguage="en";
var check="";
var titleLen=50;
var linkLen=25;

//for(var i=0;i<n.length;i++){
 /*    if(n[3].match("go")){
        getCountry=  "us";
    }
	else
	{
		var m=  n[3];
		if(m.match("_")){
			m= m.split("_")
			getCountry=m[1]
		}else{
			m= m.split("-")
			getCountry=m[1]
		}
	} */
//}
 for(var i=0;i<n.length;i++){
    if(n[i].match("go")){
        countLoc=check=  n[i-1];
		break;
    }
}
if(countLoc.match("_")){    
check= countLoc.split("_")
}else{
check= countLoc.split("-")
}
if(check.length<2){
    getCountry=check;
	if(getCountry==""){
		getCountry=  "us";
		titleLen=50;
	}
}else{
    getCountry=check[1];
    getLanguage=check[0];
	if(getCountry!="us"){titleLen=15;linkLen=15;}

}
/*Get Country and language*/
var data=englishPageTitle;//pageTitle;
var url=$(location).attr('href');
data = data.replace(/[`‐~!@#$®™%^*()_|+"\-=?;–'“”’,.<>\{\}\[\]\\\/]/gi, ' ');
data = data.replace(/\s{2,}/g,' ');
data = data.toLowerCase();
items = data.split(":");
count = items.length;
var activeLinkText="top";
var isTabClicked=false;
var desktopType="";
var orientation="landscape";
var screenSize;
var delay=4000;
var gInternalSearchFilter="";
var leadFormName="";
var lID = "";
pageTitle = pageTitle.replace(/[`‐~!@#$®™%^*()_|+"\-=?;–'“”’,.<>\{\}\[\]\\\/]/gi, ' ');

	if (window.outerWidth < 768 && /Mobi/.test(navigator.userAgent)) 
		{
			desktopType="mobile";
		}
	else if(window.outerWidth < 992 && /Mobi/.test(navigator.userAgent))
		{
			desktopType="tablet";
		}
	else if(/Mobi/.test(navigator.userAgent))
		{
			desktopType="tablet";
		}
	else
		{
			desktopType="desktop";
		}

	if (screen.height < screen.width) {
			orientation="landscape";
		}
	else{
			orientation="portrait";
		}

var screenSize = screen.width+"x" +screen.height;

	for(index=0;index<count;index++)
		{
		 if(index>0 && index<(count-1) )
		  {
			switch(index)
			{
				case 1:
					primaryCategory=$.trim(items[index]);
					break;
				  case 2:
					subSection=$.trim(items[index]);
					break;
			}
		  }
		}
		
	if(primaryCategory == "")
		primaryCategory=data;

	if(subSection == "" && items.length>=3)
		subSection=$.trim(pageTitle);

	//  error page tracking out of ready due to page load event challenges in DTM
function isErrorPage()
	{
		var url=pageTitle;
		if(url.indexOf("404")>-1 || url.indexOf("error")>-1)
			return true;
		else
			false;
	}

	
	// function isContactUsPage()
	// {
		// var url=document.title.toLowerCase();
		// if(url.indexOf("contact us")>-1)
			// return true;
		// else
			// false;
	// }

if(isErrorPage())
	{
		digitalData.page={
				pageInfo:{
				pageName: $.trim(window.location.pathname),
				pageType: "errorPage",
				pageLoadEvent: "404 error",
				errorMessage: "page not found",
				hier1: "404 error page"
				},
				category:{
				primaryCategory: "404 error page",
				}
			}

		digitalData.site={
			siteInfo:{
			language:getLanguage,
			server:"dt-" + getCountry,
			country:getCountry+":404error"
			},
			dimensions:{
			deviceType:desktopType,
			screensize:screenSize,
			Orientation:orientation
			}
		}
		digitalData.user={
			userInfo:{
				authStatus:"guest"
			}
		}
	}

	var url=pageTitle.toLowerCase(); //$(location).attr('href');
	if(!(url.indexOf("404")>-1 || url.indexOf("error")>-1 || url.indexOf("contact us")>-1))
	{	
		primaryCategory = $.trim(primaryCategory.toLowerCase());
		if(primaryCategory != "home" && primaryCategory != "data management" && primaryCategory != "data governance" && primaryCategory != "data mobility" && primaryCategory != "data analytics" && primaryCategory != "enterprise cloud" && primaryCategory != "agile i t environment")
		{
			subSection=data;
			pSection= pathLoc.split("/");
			primaryCategory=pSection[pSection.length-2].replace(/[`‐~!@#$®™%^*()_|+"\-=?;–'“”’,.<>\{\}\[\]\\\/]/gi, ' ');
		}
		digitalData.page=
			{
				pageInfo:{
				pageName: $.trim(pageTitle),
				pageType: $.trim(primaryCategory),
				hier1: $.trim(data)
				},
				category:{
				primaryCategory: $.trim(primaryCategory)
				}
			}
		digitalData.site={
			siteInfo:{
			language:getLanguage,
			server:"dt-" + getCountry,
			country:getCountry
			},
			dimensions:{
			deviceType:desktopType,
			screensize:screenSize,
			Orientation:orientation
			}
		}
		digitalData.user={
			userInfo:{
				authStatus:"guest"
			}
		}
	}

	if(subSection!="" && subSection.length>0)
		{
			digitalData["page"]["category"]["subSection"]=subSection;
			
		}
	// Top Menu - tracking start
	// HDS.com and share this page
	$(".hds-quick-navigation").each(function() 
	{
		 var listitem = $(this).find("a");
		 if( listitem.length>0)
		 {
			 listitem.each(function() {
			 var linktext = $(this).text();
			  linktext = $.trim(linktext);
				$(this).click(function()
				{
					if (linktext == ""){linktext=$(this).find("img").attr("alt");}
					globalMenuClick("linkclick","dt-"+getCountry+">tm>"+linktext.toLowerCase(),pageTitle,"link","dt-"+getCountry+">top menu"); });
				$(this).mousedown(function(e){
					if(e.which == 3){
						globalMenuClick("linkclick","dt-"+getCountry+">tm>"+linktext.toLowerCase(),pageTitle,"link","dt-"+getCountry+">top menu");
					} 			
				});
			});                 
		 }
    });	
	
	//header HDS hitachi logo
	$(".hitachi-logo").on("click", function() {
		globalMenuClick("linkclick","dt-"+getCountry+">tm>hitachi logo",pageTitle,"link","dt-"+getCountry+">top menu");
	});
	// Top Menu - tracking end
	
	// Main Menu - tracking start
	//Header Main Menu Links Custom Tracking
	$(".globalNavWrapper").each(function() {
     var listitem = $(this).find("a");
      if( listitem.length>0)
	 {
		 listitem.each(function() {
			 var linktext = $(this).text();
			 linktext=$.trim(linktext);
             if(linktext=="" || linktext.length==0)
             {linktext= $(this).children().text();}
            $(this).click(function(){
                var triggerName="dt-"+getCountry+">mm>"+linktext.toLowerCase();
                globalMenuClick("linkclick",triggerName,pageTitle,"link","dt-"+getCountry+">main menu"); 
            });
			$(this).mousedown(function(e){
				if(e.which == 3){
                var triggerName="dt-"+getCountry+">mm>"+linktext.toLowerCase();
                globalMenuClick("linkclick",triggerName,pageTitle,"link","dt-"+getCountry+">main menu"); 
				} 			
			});
		});                 
     }
    });	
	
	//header HDS logo
	$(".hds-main-navigation-container").each(function() {
		var listitem = $(this).find("a");
		listitem.each(function() {
			$(this).click(function(){
				if($(this).attr("href").indexOf("home.html")>-1)
				globalMenuClick("linkclick","dt-"+getCountry+">mm>hds logo",pageTitle,"link","dt-"+getCountry+">main menu");
			});	
		});
	});		
	// Main Menu - tracking end
	
	//Hero Banner - all pages tracking start
	$(".bannerSectionImage").each(function() {
     
		 var listitem = $(this).find("a");
		 var url=$(location).attr('href');
		 var linktext = "";
		 var linkposition="";
		 var eType="link";
		 var eClass="";
		 var linkType="hero-";
		 var linkpanel = "";
		 if( listitem.length>0)
		 {
			listitem.each(function() {
				$(this).click(function(){
					//linkpanel = $.trim($(this).parents('.bannerSectionImage').find('h2').text());
					linktext = "dt-"+getCountry+">hero-" + $.trim($(this).parents('.bannerSectionImage').find('h2').text());
					linkposition="dt-"+getCountry+">hero-" + getPageNameFromURL();					
					var eClass=$(this).attr("class");
					if(eClass == undefined ||!eClass || eClass == 'isGatedLock'){eClass = $(this).parent().attr("class");}
					if (eClass.indexOf("playVideoBox")!= -1){eType = "play icon";if(!(linktext.indexOf("-play icon")>-1)){linktext=linktext + "-play icon";}} 
					else if ( eClass.indexOf("watchVideoBtn")!= -1)
					{eType = "button";if(!($(this).text()>-1)){linktext=linktext + "-" + $.trim($(this).text());linkposition="dt-"+getCountry+">hero-"  + getPageNameFromURL();}} 
					globalMenuClick("linkclick",linktext.toLowerCase(),pageTitle,eType,linkposition);
				});
			});                 
		 }
    });
	
	//Hero Banner - all pages tracking end
	//Hexagon banner  start
	//home page (3 hex)
	$(".hexagon-dt").each(function() {
     var listitem = $(this).find("a");
	 var pName=getPageNameFromURL();
	 var hexPanelName = shortTxtLen($.trim($(this).parents(".business-transform-hexagon-list").prev().find('h2').text()),titleLen);
     if( listitem.length>0)
	 {
		 listitem.each(function() {
				$(this).click(function(){
					var offertitle = shortTxtLen($(this).parent().parent().find("h4").text(),titleLen);
					globalMenuClick("linkclick","dt-"+getCountry+">hex 3-" + hexPanelName.toLowerCase() + "-" + offertitle.toLowerCase() + "-" + shortTxtLen($.trim($(this).text()),linkLen),pageTitle,"link","dt-"+getCountry+">hex 3-" + hexPanelName + "-" + pName); });
		});                 
     }
    });
	// home page
	//data governance, agile-it, data-analytics (1 on each page)
	$(".hexagon-transformative").each(function() {
     var listitem = $(this).find("a");
	 var pName=getPageNameFromURL();
	 var hexPanelName = shortTxtLen($.trim($(this).parents(".business-specific-container").find('h2').text()),titleLen);
     if( listitem.length>0)
	 {
		 listitem.each(function() {
 				$(this).click(function(){
					//var linktext = "hex-" + hexPanelName + "-" + $.trim($(this).parents(".hexagon-transformative").find('h4').text()) + "-" + $.trim($(this).text());
					var linktext = shortTxtLen($.trim($(this).parents(".hexagon-transformative").find('h4').text()),titleLen) + "-" + shortTxtLen($.trim($(this).text()),linkLen);
					linktext = "dt-"+getCountry+">hex right-" + linktext;
					globalMenuClick("linkclick",linktext.toLowerCase(),pageTitle,"link","dt-"+getCountry+">hex right-" + hexPanelName + "-" + pName); });
		});                 
     }
    });
	// data governance, agile-it, data-analytics
	//Hexagon banner end
	
	// Hexagon Popup link level tracking
	$(document).on('click','.dt-transform-overlay a',function(){
		var getClass=$(this).attr('class');
		var panelType="dt-"+getCountry+">hex";
		var titletext=$(this).parents(".dt-transform-overlay").find("h2").text();
		var eType="link";
		var linkcomptext="";
		var linktext = $.trim($(this).text());
		if(getClass=="close-dt-overlay"){
			linktext = "x close overlay";
		}
		linkcomptext= panelType + "-" + titletext.toLowerCase() + "-" + linktext.toLowerCase();
		globalMenuClick("linkclick", linkcomptext.toLowerCase(), pageTitle, eType, panelType + "-" + getPageNameFromURL());		
	});
	
	
	// Hexagon in footer link level tracking
	$(document).on('click','.behind-scene-hexagon a',function(){
		var panelType="dt-"+getCountry+">hex";
		var hexPanelName=$(this).parents(".behind-scene-hexagon").find("h2").text();
		var titletext=$(this).parent().parent().find("h4").text();
		var eType="link";
		var linkcomptext="";
		var linktext = $.trim($(this).text());
		linkcomptext= panelType + "-" + hexPanelName + "-" + titletext.toLowerCase() + "-" + linktext.toLowerCase();
		globalMenuClick("linkclick", linkcomptext, pageTitle, eType, panelType + "-" + hexPanelName + "-" + getPageNameFromURL());		
	});
	
	// Box panel start
	//home, data governance, data mobility, data analytics, agile it
	$(".about-hds-articles").each(function() {
     var listitem = $(this).find("a");
	 var pName=getPageNameFromURL();
	 var boxPanelName = shortTxtLen($.trim($(this).find("h2").text()),titleLen);
     if( listitem.length>0)
	 {
		 listitem.each(function() {
 				$(this).click(function(){
					//var linktext = "hex-" + boxPanelName + "-" + $.trim($(this).parents(".hexagon-transformative").find('h4').text()) + "-" + $.trim($(this).text());
					var linktext = shortTxtLen($.trim($(this).parents(".spotlight-content").find(".spotlight-title").text()),titleLen) + "-" + shortTxtLen($.trim($(this).text()),linkLen);
					linktext = "dt-"+getCountry+">panel-recom reading-" + linktext
					globalMenuClick("linkclick",linktext.toLowerCase(),pageTitle,"link","dt-"+getCountry+">panel-recom reading-" + boxPanelName + "-" + pName); });
		});                 
     }
    });
	// home, data governance, data mobility, data analytics, agile it

	//data management
	$(".news-insight-resources").each(function() {
     var listitem = $(this).find("a.animateLink");
	 var pName=getPageNameFromURL();
	 var boxPanelName = shortTxtLen($.trim($(this).find("h3").text()),linkLen);
     if( listitem.length>0)
	 {
		 listitem.each(function() {
 				$(this).click(function(){
					//var linktext = "hex-" + boxPanelName + "-" + $.trim($(this).parents(".hexagon-transformative").find('h4').text()) + "-" + $.trim($(this).text());
					var linktext =  shortTxtLen($.trim($(this).parents(".news-resources-col").find(".headline.hidden-xs").text()),titleLen) + "-" + shortTxtLen($.trim($(this).text()),linkLen);
					linktext = "dt-"+getCountry+">box-" + linktext;
					globalMenuClick("linkclick",linktext.toLowerCase(),pageTitle,"link","dt-"+getCountry+">box-panel-" + boxPanelName + "-" + pName); 
					});
		});                 
     }
    });
	//data management	
	// Box panel end
	
	//panel1- enterprise Panel on home page 'TRANSFORM YOUR DATA TO TRANSFORM YOUR BUSINESS' 
	$(".enterprise-cloud").each(function() {
     var listitem = $(this).find("a");
	 var pName=getPageNameFromURL();
	 var nPanelName = shortTxtLen($.trim($(this).find("h2").text()),linkLen);
	 
     if( listitem.length>0)
	 {
		 listitem.each(function() {
 				$(this).click(function(){
					var offertitle = shortTxtLen($.trim($(this).parent().parent().find('h3').text()),titleLen); 
					var linktext = nPanelName + "-" + offertitle + "-" + shortTxtLen($.trim($(this).text()),linkLen);
					linktext = "dt-"+getCountry+">panel-ent cloud-" + linktext;
					globalMenuClick("linkclick",linktext.toLowerCase(),pageTitle,"link","dt-"+getCountry+">panel-ent cloud-" + nPanelName + "-" + pName); });
		});                 
     }
    });
	
	//panel2 New Panel on home page 'TRANSFORM YOUR DATA TO TRANSFORM YOUR BUSINESS' end	
	$(".news-insight-explore").each(function() {
     var listitem = $(this).find("a.animateLink");
	 var pName=getPageNameFromURL();
	 var nPanelName = "";
	 var linktext="";
     if( listitem.length>0)
	 {
		 listitem.each(function() {
 				$(this).click(function(){
					nPanelName = shortTxtLen($.trim($(".news-insight-explore").find(".heading").text()),linkLen);
					var offertitle = shortTxtLen($.trim($(this).text()),titleLen); 
					if(offertitle.toLowerCase().indexOf("read more")>-1){
						offertitle = shortTxtLen($.trim($(this).parent().prev().text()),titleLen);
						linktext = "dt-"+getCountry+">panel-news insight-" + nPanelName + "-" + offertitle + "-" + shortTxtLen($.trim($(this).text()),linkLen);
					}
					else
					{
						linktext = "dt-"+getCountry+">panel-news insight-" + nPanelName + "-" + shortTxtLen($.trim($(this).text()),linkLen);
			           
					}
				
					
					//var linktext = "dt-"+getCountry+">panel-" + $.trim($(this).parents(".news-resources-col").find(".headline.hidden-xs").text()) + "-" + $.trim($(this).text());
					globalMenuClick("linkclick",linktext.toLowerCase(),pageTitle,"link","dt-"+getCountry+">panel-news insight-" + nPanelName + "-" + pName); 
				});
		});                 
     }
    });
	// Footer - Grey Strip tracking start
	//Footer links tracking
	$("div.footer-white").each(function() {
		var links = $(this).find("a");
	   links.each(function() {
		 var linktext = $(this).text();
		 linktext=$.trim(linktext);
           if(linktext!="" || linktext.length > 0)
		 {
		$(this).click(function(){globalMenuClick("linkclick","dt-"+getCountry+">ft>"+linktext.toLowerCase(),pageTitle,"link","dt-"+getCountry+">footer"); });
		$(this).mousedown(function(e){
			if(e.which == 3){
				globalMenuClick("linkclick","dt-"+getCountry+">ft>"+linktext.toLowerCase(),pageTitle,"link","dt-"+getCountry+">footer");
			} 			
			});
		 }
	  })

	});	
	// Footer - Grey Strip tracking end
	// Footer - Black Strip tracking start
	//text logo
	$(".footer-logo").on("click", function() {
		globalMenuClick("linkclick","dt-"+getCountry+">ft>hitachi logo",pageTitle,"link","dt-"+getCountry+">footer");
	});	
	
	//social media links
	$(".social-icons").each(function() {
		var links = $(this).find("img");
	   links.each(function() {
		 var linktext = $(this).attr("alt");
		 linktext=$.trim(linktext);
         if(linktext!="" || linktext.length > 0)
		 {
			$(this).click(function(){globalMenuClick("linkclick","dt-"+getCountry+">ft>"+linktext.toLowerCase(),pageTitle,"link","dt-"+getCountry+">footer"); });
			$(this).mousedown(function(e){
			if(e.which == 3){
				globalMenuClick("linkclick","dt-"+getCountry+">ft>"+linktext.toLowerCase(),pageTitle,"link","dt-"+getCountry+">footer");
				} 			
			});
		 }
	  })

	}); 
	// Footer - Black Strip tracking end
	// Footer - Blue Strip tracking start
	//Click to call tracking
	$('.talk .call').click(function(){
		//if(window.outerWidth < 992){
			var pNumber="dt-"+getCountry+">ph no - " + $(this).text();
			clicktocall(pNumber,pageTitle);
		//}
	})
	
	function clicktocall(pNumber,pageTitle){
    digitalData.eventData= {
    	eventName:'click to call',
		eventAction:pNumber,
		eventPage:pageTitle
    }
    _satellite.track('clicktocall');
	}
	
	//local phone numbers CTAs
	$(".view-phone .reseller").on("click", function() {
		var links = $(this).find("a");
		var linktype="link";	  
		var linkhref= $(this).text(); 
		linkhref = linkhref.replace(/[`‐~!@#$®™%^*()_|+"\-=?;–'“”’,.<>\{\}\[\]\\\/]/gi, '');
		linkhref = $.trim(linkhref);
		globalMenuClick("linkclick","dt-"+getCountry+">ft>"+linkhref.toLowerCase(),pageTitle,linktype,"dt-"+getCountry+">footer");
	});	
	
	//footer contact us button
	
	$("div.buttons").each(function() {
		var links = $(this).find("a");
		var linktype="";
	   links.each(function() {
		 
		$(this).click(function(){
			var linkhref = $(this).attr('href');
           if(linkhref.indexOf("/partnerlocator/en_us/partnerlocator.html") > -1)
		 {
		 	linkhref= "find an hds partner";
			linktype="link";
		 }
		 else
		 {
			linkhref= "contact sales"; 
			linktype="button";
		 }
			globalMenuClick("linkclick","dt-"+getCountry+">ft>"+linkhref.toLowerCase(),pageTitle,linktype,"dt-"+getCountry+">footer"); 
			});
		$(this).mousedown(function(e){
			if(e.which == 3){
				var linkhref = $(this).attr('href');
				   if(linkhref.indexOf("/partnerlocator/en_us/partnerlocator.html") > -1)
				 {
					linkhref= "find an hds partner";
					linktype="link";
				 }
				 else
				 {
					linkhref= "contact sales"; 
					linktype="button";
				 }
					globalMenuClick("linkclick","dt-"+getCountry+">ft>"+linkhref.toLowerCase(),pageTitle,linktype,"dt-"+getCountry+">footer"); 
			} 			
		});
	  })

	});		
	// Footer - Blue Strip tracking end

	// Return to top link
	$(".text-return").on("click", function() {
		globalMenuClick("linkclick","dt-"+getCountry+">return to top",pageTitle,"button","dt-"+getCountry+">return to top");
	});		
	
	//cookie session storage variable created on click on gated assets
	$(".isGatedLock").on("click", function(){
			document.cookie="dtGatedParentPageRef="+window.location.href+";path=/;domain=.hds.com";
			//$.cookie("dtGatedParentPageRef", window.location.href, { path: '/', domain='.hds.com' });
	}); 

	//globalMenuClick(eventname,triggername,page)
	function globalMenuClick(eventName,triggerName,page,triggerType,Position)
	{
		digitalData.eventData= {
		eventName:eventName,
		eventAction:triggerName,
		eventPage:page,
		eventType:triggerType,
		eventPostion:Position
		} 
   // alert("link clicked----"+JSON.stringify(digitalData.eventData));
    _satellite.track('globalMenuClick');
	}

	// Get page name from URL for primary part
	function getPageNameFromURL()
	{
		var fileName = window.location.pathname;
		var n=fileName.lastIndexOf("/");
		fileName=fileName.substring(n+1, fileName.length);
		n=fileName.lastIndexOf(".");
		if(n>0){fileName=fileName.substring(0, n);}
		return fileName;
	}
		
	function videoTracking(vId, pPageName)
	{
		digitalData.eventData= {
			videoId:vId,
			parentPage:pPageName
		} 
		_satellite.track('videotracking');

	}
	
	// email links clicks---------mailto should not be implemented on the site------so disabled this tracking
	$('a[href^="mailto:"]').click(function() {
			var linktxt=$(this).text();
			var linkhref=$(this).attr("href");
			if(linktxt.indexOf('@')>0 || linkhref.indexOf('@')>0){globalMenuClick("linkclick","dt-"+getCountry+">em-"+linktxt.toLowerCase(),pageTitle,"email","dt-"+getCountry+">panel-main"); }
	});

	// email links clicks
	$('a[rel="emailHome"]').click(function() {
			var linktxt=$(this).text();
			var linkhref=$(this).attr("href");
			if(linktxt.indexOf('@')>0 || linkhref.indexOf('@')>0){globalMenuClick("linkclick","dt-"+getCountry+">em-"+linktxt.toLowerCase(),pageTitle,"email","dt-"+getCountry+">panel-main"); }
	});
	
	// new tabs on home page
	$(".PagerBar").each(function() {
	 	var links = $(this).find("a")
       links.each(function() {
		 	$(this).click(function(){
                isTabClicked=true;
                var tabTitle = $.trim($(this).text()).toLowerCase().replace(/\s/g," ");
				tabTitle= shortTxtLen(tabTitle,titleLen);
				tabTitle = "tab-"+tabTitle+"-button";
				//var tabTitle = "tab-"+getCountry+"-"+$.trim($(this).text()).toLowerCase().replace(/\s/g," ")+"-button";
                tabClick(primaryCategory,tabTitle,pageTitle,"Tabclick"); 
            });
	  	});

	});
	
	function tabClick(primaryCategory,tabName,pageName,eventName)
	{
        digitalData.event=[];
        digitalData.event.push({
			category:{
			primaryCategory:primaryCategory
			},
			eventInfo:{ 
				eventName:eventName
			},
			tabInfo:{
                tabName: tabName,
                pageName: pageName
                }
			})
		_satellite.track('Tab Click');
	}
	//panel3 Resources Panel link level tracking
	$(document).on('click','.resources-container a',function(){
		var eClass=$(this).parent().attr("class");
		var panelType= $(".resources-container").find(".resources-title").text().toLowerCase();
		panelType="dt-"+getCountry+">panel-" + shortTxtLen(panelType,titleLen);
		//var offertext=$(this).parents(".resources-category").find(".resources-category-heading").text();
		var titletext=$(this).parents(".resources-category-box").find(".resources-category-title").text();
		var linktext=$(this).text();
		var eType="link";
		var linkcomptext="";
		//offertext = $.trim(offertext);
		titletext = shortTxtLen($.trim(titletext),titleLen);
		linktext = shortTxtLen($.trim(linktext),linkLen);
		linkcomptext= panelType + "-" + titletext.toLowerCase() + "-" + linktext.toLowerCase();
		if(eClass.indexOf("cs-selection-box")>-1){linkcomptext= panelType + "-" + linktext.toLowerCase();eType="button";}
		globalMenuClick("linkclick", linkcomptext, pageTitle, eType, panelType + "-" + getPageNameFromURL());		
	});
	// short link text length
	function shortTxtLen(lnkText,tLen)
		{
			var txtLen=lnkText.length;
			var tempLen=47;
		  if(tLen=="15"){tempLen=12;}	  
			if(txtLen>tLen)lnkText=lnkText.substring(0,tempLen) + "...";
			return lnkText;
		}