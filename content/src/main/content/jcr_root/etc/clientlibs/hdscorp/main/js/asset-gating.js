/*Global Namespace*/
var hds = window.hds || {};
var parenRef = "";
var assetDesc = "";
var moretext = "";
var lesstext = "";

(function(window, document, $, hds) {
    hds.assetGating = {
        init: function() {
            parenRef=document.referrer;
            hds.assetGating.bindEventsGating();
            hds.assetGating.pdfDescOrientation();
            hds.assetGating.showMoreDescription();
         },
         downloadPdf:function(){
            var randNum="";
            var redirectUrl="";
            var extUrl =$("div.externalurl").attr("data-external-url");
            if(extUrl!=null && extUrl.length>0)
                redirectUrl=extUrl
            else {
                redirectUrl=window.location.href;
                if(redirectUrl.indexOf("?q=1")!=-1 || redirectUrl.indexOf("&q=1")!=-1){

                }
                else{
                    if(redirectUrl.indexOf("?")==-1){
                        randNum="?q=1"
                    }
                    else{
                        randNum="&q=1"
                    }
                }
                redirectUrl=redirectUrl+randNum
            }

            if (/MSIE (\d+\.\d+);/.test(navigator.userAgent) || (navigator.appVersion.indexOf("MSIE 10") !== -1) || (navigator.userAgent.indexOf("Trident") !== -1 && navigator.userAgent.indexOf("rv:11") !== -1) || window.navigator.userAgent.indexOf("Edge") > -1)
            {
                var referLink = document.createElement('a');
                referLink.href = redirectUrl;
                referLink.style.display = 'none';
                parent.document.body.appendChild(referLink);
                referLink.click();
            }
            else{
                window.open(redirectUrl, '_blank');
            }
         },
         setVerticalCenter: function(){
            var h1 = parseInt($('.gated-container').height()) + parseInt($('.main-title').outerHeight()) + parseInt($('.gated-asset-title').css('padding-bottom'));
            var h2 = $('.gatted-asset-form').outerHeight();
            if ($(window).width() > 991) {
                if(h2 <= h1){
                    h2 = h1 + 80;
                    var pb = (h2 - h1) + 80;
                    $('.gatted-asset-form').css({'height':h2});
                    $('.gatted-asset-pg').css({'padding-bottom':pb});
                }else{
                    var pb = (h2 - h1) + 80;
                    $('.gatted-asset-pg').css({'padding-bottom':pb});
                }
                if($(".main-title:visible").length === 0){
                    $('.gatted-asset-pg').css({'padding-bottom':0});
                }
            }
         },
         pdfDescOrientation: function(){
            if ($(window).width() < 991) {
                if (hds.resourceLib._isEmpty($('.asset-desc-devices'))) {
                    assetDesc = $('.gated-container .gated_asset').html();
                    $('.asset-desc-devices').html(assetDesc);
                    $('.gated-container .gated_asset').html("");
                    $('.gatted-asset-form').css({'height':''});
                    $('.gatted-asset-pg').css({'padding-bottom':''});                    
                }
            }else{
                if(assetDesc != ""){
                    hds.assetGating.showMoreDescription();
                }
            }
         },
         showMoreDescription: function(){
            var showChar = 200;
            var ellipsestext = "...";            
            
            if ($(window).width() < 991) {
                var content = $('.asset-desc-devices div.gate-more-less').html();
                if (!hds.resourceLib._isEmpty($('.asset-desc-devices'))) {
                    if(content.length > showChar) {
                        var c = content.substr(0, showChar);
                        var h = content.substr(showChar-0, content.length - showChar);
                        var html = c + '<span class="moreelipses">'+ellipsestext+'</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="javascript:void(0);" class="morelink">'+moretext+'</a></span>';
                        $('.asset-desc-devices div.gate-more-less').html(html);
                    }
                }
            }else{
                if(assetDesc != ""){
                    $('.gated-container .gated_asset').html(assetDesc);
                    $('.asset-desc-devices').html("");
                }
            }            
         },
         setAssetDefaultImg: function(){
            $('.gated-asset-img').addClass('default');
            $('#default-gated-img').css({'display':'block'});
         },
         bindEventsGating: function(){
            moretext = $('.pdf-desc-more').text();
            lesstext = $('.pdf-desc-less').text();
            $('.breadcrumb-container .breadcrumb').removeClass('black');
            $('.hds-main-navigation-container').removeClass('navwithouthero');

            if ($('#default-gated-img').length > 0){
                hds.assetGating.setAssetDefaultImg();
            }           

            $(window).resize(function() {
                hds.assetGating.setVerticalCenter();
            });

            $( window ).on( "orientationchange", function( event ) {
                hds.assetGating.pdfDescOrientation();
                hds.assetGating.setVerticalCenter();
                $('.disclaimer-desc').css({'display':'none'});
            });

             $(document).on('click', '.morelink', function(){
                if($(this).hasClass("less")) {
                    $(this).removeClass("less");
                    $(this).html(moretext);
                    $('html, body').scrollTo('.gated-pdf-desc',{duration:'slow'});
                } else {
                    $(this).addClass("less");
                    $(this).html(lesstext);
                }
                $(this).parent().prev().toggle();
                $(this).prev().toggle();
                return false;
            });

            /* Legal Disclaimer */
            $(document).on('click','.disclaimer-link a', function(){
                $('.disclaimer-desc').css({'display':'block'});
                var dTop = $('.disclaimer-link').offset().top;
                $('.disclaimer-desc').css({'top':dTop + 20});
                $('html, body').animate({scrollTop: $(".gated-asset-disclaimer").offset().top}, 500);
            })

            $(document).on('click','.disclaimer-desc .close', function(){
                $(this).parent().css({'display':'none'});
            })
         }
    }
}(window, document, jQuery, hds));
$(function() {
    if ( $('.gated_asset').length > 0){
        hds.assetGating.init();
    }
})