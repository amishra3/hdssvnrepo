<%@include file="/apps/foundation/global.jsp"%>
<%@page session="false"%>

<div class="news-insight-explore">
	<div class="content-container container-fluid">

 
        <div class="heading">${properties.exploreinsightstitle}</div>
				<c:forEach var="count" begin="1" end="${properties.numberofcolumns}"
					varStatus="status">
<div class="col-sm-4">
					<cq:include path="exprloreinsigthsbigtile${status.count}"
						resourceType="hdscorp/components/content/exploreinsightsfromdtleaderscontainer/exploreinsightsdtleadersverticaltiles" />
</div>
				</c:forEach>



		</div>
	</div>