<%--
PDF Service component.
--%>

<%@page import="com.hdscorp.cms.config.HdsCorpGlobalConfiguration"%>
<%@include file="/apps/foundation/global.jsp"%>
<%@page import="com.hdscorp.cms.util.JcrUtilService"%>
<%@page session="false" %>

<%
String pdfPath= request.getParameter("pdfPath");
request.setAttribute("pdfPath", pdfPath);

String requestURI = (String)request.getRequestURI();
String pdfRenditionImagePath = (String)HdsCorpGlobalConfiguration.getPropertyValue(HdsCorpGlobalConfiguration.ASSET_GATING_IMAGE_RENDITION);
String pdfImageRendition = requestURI+pdfRenditionImagePath; 
pageContext.setAttribute("pdfImageRendition", pdfImageRendition);
%>



<sling:adaptTo adaptable="${slingRequest}" adaptTo="com.hdscorp.cms.slingmodels.PDFMetaModel" var="pdfMetaModel" />

<sling:adaptTo adaptable="${resource}" adaptTo="com.hdscorp.cms.slingmodels.AssetGettingModel" var="assetGettingModel" />

<c:set var="pdfNode" value="${pdfMetaModel.pdfNode}" /> 

<c:set var="pdfTitle" value="${pdfNode.title}" scope="request"/>
<c:set var="pdfDesc" value="${pdfNode.description}" scope="request"/>

<c:set var="formIframeURL" value="${properties.formIframeURL}"/>
<c:if test="${empty formIframeURL}">
<c:set var="formIframeURL" value="https://pages.hds.com/hds-gated-form.html"/>
</c:if>


<c:set var="pdfimagelocation" value="${assetGettingModel.pdfimagelocation}"/>

<%
String pdfName=null;
    String pdfCurrentPath = request.getParameter("pdfPath");  
if(pdfCurrentPath==null){
      pdfCurrentPath = request.getRequestURI();
  }

    pdfName=pdfCurrentPath.substring(pdfCurrentPath.lastIndexOf("/")+1);

    pdfName=pdfName.replace(".pdf",".jpg");
    pdfName=pdfName.replace(".html",".jpg");


Resource resources = JcrUtilService.getResourceResolver().resolve(pageContext.getAttribute("pdfimagelocation")+"/"+pdfName);

%>

<c:if test="${null!=pdfNode}">
 <div class="pdf-desc-less">${assetGettingModel.pdfmobileshowless}</div>
<div class="pdf-desc-more">${assetGettingModel.pdfmobileloadmore}</div>
<div class="gatted-asset-pg" style="background-image: url('${properties.pdfassetgatingbanner}')">
    <div class="row content-container gated-asset-title">
        <div class="col-md-7">
        <h1 class="main-title">${pdfNode.title}</h1>
<div class="gated-content-type"> ${pdfNode.contentType}</div>
        <div class="gated-asset-thanks">
                    <h1>${assetGettingModel.pdfsucessheading}</h1>
          <p>${assetGettingModel.pdfsucessmessage}</p>
        </div>
      </div>

        <div class="col-md-5 asset-down-container">
            <div class="gated-asset-download clearfix">
                <div class="gated-asset-subtitle">
                  <span>${pdfNode.contentType}</span> ${pdfNode.subTitle} 
                </div>
                <div class="btn-square-red download-cta hidden-xs hidden-sm">
                    <a class="animateLink download-pdf" target="_self" href="javascript:void(0);" onclick="hds.assetGating.downloadPdf();" title="${assetGettingModel.pdfdonwloadnow}">${assetGettingModel.pdfdonwloadnow}<span class="glyphicon glyphicon-menu-right animateIcon"></span></a>
                </div>
                <div class="share"><cq:include path="sharethismobile" resourceType="hdscorp/components/content/pdfsharethispage" /></div>
            </div>
            <div class="btn-square-red download-cta hidden-md hidden-lg">
                <a class="animateLink download-pdf" target="_self" href="javascript:void(0);" onclick="hds.assetGating.downloadPdf();" title="${assetGettingModel.pdfdonwloadnow}">${assetGettingModel.pdfdonwloadnow}<span class="glyphicon glyphicon-menu-right animateIcon"></span></a>
            </div>
        </div>
        <div class="col-md-5 grey-bg">
              <div class="gatted-asset-form clearfix">                        
                  <div class="form">
                      <div class="register-access">${assetGettingModel.message}</div>
                      <div class="gated-asset-subtitle">
                          <span> ${pdfNode.contentType}</span> ${pdfNode.subTitle}
                      </div>
                      <div class="gated-asset-img">
                        <% 
                           if (!resources.isResourceType(Resource.RESOURCE_TYPE_NON_EXISTING)) { %>

                                <img src="<%=pageContext.getAttribute("pdfimagelocation")+"/"+pdfName%>" alt="${pdfNode.subTitle}" title="${pdfNode.subTitle}" class="img-responsive">
                <% } else {%>
                                <img src="${assetGettingModel.pdfdefaultimage}" alt="${pdfNode.subTitle}" title="${pdfNode.subTitle}" class="img-responsive" id="default-gated-img">
                <%}%>
                      </div>
                        <div class="asset-desc-devices"></div>
                      <div class="asset-form">
                          <div id="gated-pdf-loader"></div>
                          <iframe src="${formIframeURL}" id="marketo_Iframe" style="border:none;" onload='gatedPdfIframeHeight(this.id)'></iframe>
                      </div>                        
                      
                     <c:if test="${not empty pdfNode.ctaLinkoption}">
                        <div class="gated-asset-disclaimer">
                            <div class="disclaimer-link"><a href="javascript:void(0);" class="animateLink">${pdfNode.ctaLinktitle}<span aria-hidden="true" class="glyphicon glyphicon-menu-right animateIcon"></span></a></div>            
                        </div>
                      </c:if>
                  </div>
              </div>
          </div>
    </div>
    <div class="gated-container">        
        <div class="row content-container">
          <div class="col-md-7 gated_asset">
              <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 col-no-pad"> 
                  <!--<c:if test="${not empty pdfNode.createdDate}">
                      <h3>${pdfNode.createdDate}</h3>
                  </c:if>-->
                  <span></span>
              </div>
              <c:if test="${not empty pdfNode.imagePath}">
                  <div class="gated-heading col-sm-12">                            
                      <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 col-no-pad">
                          <img src="${pdfNode.imagePath}" alt="" class="img-responsive">
                      </div>
                  </div>
              </c:if>
              <div class="gated-pdf-desc">
              <c:if test="${not empty  pdfNode.longDescription}">
                <div class="gate-more-less">${pdfNode.longDescription}</div>
              </c:if>
              <c:if test="${empty  pdfNode.longDescription}">
                <div class="gate-more-less">${pdfNode.description}</div>
              </c:if>                 
              <c:if test="${not empty  pdfNode.externalContentURL}">
                  <div class="hidden externalurl" data-external-url="${pdfNode.externalContentURL}"></div>
              </c:if> 

              </div>


               <div class="form-fill-msg">${properties.pdfgenericmsg}</div>                    
          </div>

          <noindex><div id="thanksmessage">${assetGettingModel.thanksyouMessage}</div></noindex>          
        </div>
  </div>
  <div class="col-md-12 col-sm-12 col-xs-12 row content-container disclaimer-desc">
    <div class="close" title="Close"></div>
    ${pdfNode.ctaLincontent}
  </div>
    </c:if>
    <cq:include path="newinsightverticalcontainer" resourceType="hdscorp/components/content/newsexplorerverticalcontainer" />
</div>