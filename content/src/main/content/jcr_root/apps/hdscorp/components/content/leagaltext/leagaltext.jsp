<%@page session="false" %>
<%@include file="/apps/foundation/global.jsp"%>
<%@include file="/apps/foundation/global.jsp"%>
<%@page import="com.hdscorp.cms.util.PageUtils"%>
<%@page import="com.hdscorp.cms.util.PathResolver"%>


      <div class="category-heading">
 <c:choose>
    <c:when test="${not empty properties.legallable && not empty applicationScope.legalLabelText}">
       <h2>${properties.legallable}  </h2>
    </c:when>
     <c:when test="${not empty properties.legallable &&  empty applicationScope.legalLabelText}">
       <h1>${properties.legallable}</h1>
    </c:when>

    <c:otherwise>      
    </c:otherwise>
</c:choose>
      </div>  	

${properties.legaldescription}   