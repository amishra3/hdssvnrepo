<%@include file="/apps/foundation/global.jsp"%>

<c:set var="editbarstyle" value="" />
<wcmmode:edit>
	<c:set var="editbarstyle" value="style='overflow:auto;'" />
</wcmmode:edit>

<div class="contentarea">

	<c:if test="${pageProperties.personalizationEnabled}">
	
		<cq:include path="clientcontext" resourceType="cq/personalization/components/clientcontext" />
	
	</c:if>

    <cq:include path="AssetExpiryNotification" resourceType="/apps/hdscorp/components/content/assetexpiry/assetexpiryconfig" />
    <cq:include path="AssetExpiryParsys" resourceType="foundation/components/parsys" />
	
</div>