<%@page import="org.apache.sling.api.resource.ResourceUtil"%>
<%@page import="org.apache.jackrabbit.commons.JcrUtils"%>
<%@page import="com.hdscorp.cms.util.JcrUtilService"%>
<%@page import="com.day.cq.commons.jcr.JcrUtil"%>
<%@page import="org.apache.sling.api.resource.Resource"%>
<%@page import="org.apache.sling.api.resource.ValueMap"%>
<%@page import="javax.jcr.Node"%>
<%@page import="com.hdscorp.cms.dao.JCRDataAccessor"%>
<%@page import="com.hdscorp.cms.constants.GlobalConstants"%>
<%@page session="false"%>
<%@include file="/apps/foundation/global.jsp"%>

<%@page import="com.hdscorp.cms.util.PathResolver"%>
<%@page import="com.day.cq.wcm.api.Page"%>
<%@ page import="org.apache.sling.api.resource.ResourceResolver,
					com.day.cq.wcm.commons.WCMUtils,
					com.day.cq.wcm.api.components.Toolbar"%>


<c:if test="${empty pageProperties.hidebreadcrumb}">
	<%
		//Current Node should not be included in the breadcrumb
		int currentPageDepth = currentPage.getDepth()-1;
		JCRDataAccessor dataAccessor = new JCRDataAccessor(pageContext);

		StringBuffer breadcrumbContent = new StringBuffer("");
        String productBookMarkLink = "" ;
		String rootLabel = (String) properties.get("rootLabel",
				String.class);

		String rootTargetUrl = (String) properties.get("rootUrl",
				String.class);

        Resource resourceObject = resourceResolver.resolve(rootTargetUrl);

        if(resourceObject!=null && resourceObject.getChild("jcr:content")!=null){

        Resource metaDataResourceObject = resourceObject.getChild("jcr:content");


        ValueMap resVMap = metaDataResourceObject.adaptTo(ValueMap.class);


		if (rootTargetUrl != null) {
			rootTargetUrl = PathResolver.getShortURLPath(rootTargetUrl);
		}


           String itemtitle=rootLabel;
             if(resVMap!=null && resVMap.get("analyticspagetitle", "")!=null && !resVMap.get("analyticspagetitle","").trim().equalsIgnoreCase("")){
                  itemtitle=resVMap.get("analyticspagetitle", "");
            }


		breadcrumbContent.append("<li itemprop='itemListElement' itemscope itemtype='http://schema.org/ListItem'><a itemprop='item' class='breadcrumblink' href='" + rootTargetUrl + "' itemtitle='"+itemtitle+"'><span itemprop='name'>" + rootLabel + "</span></a><meta itemprop='position' content='1' /></li>");

		Page parentPageHandle = null;
		String parentTitle = "";
		String parentPath = "";
		int count = 2 ;
		for (int i = 3; i < currentPageDepth; i++) {
			parentPageHandle = currentPage.getAbsoluteParent(i);
			if (parentPageHandle != null) {
				String parentResourcePath =  parentPageHandle.getPath();
				String hidecurrentpageinbreadcrumb = parentPageHandle.getProperties().get("hidecurrentpageinbreadcrumb", "false");
				if(hidecurrentpageinbreadcrumb == null || "false".equalsIgnoreCase(hidecurrentpageinbreadcrumb) ){
					breadcrumbContent.append(" > ");
					parentPath = PathResolver.getShortURLPath(parentResourcePath);

                     Resource resourcePageObject = resourceResolver.resolve(parentResourcePath);
                    Resource metaDataResourcePageObject = resourcePageObject.getChild("jcr:content");
                    ValueMap resPageVMap = metaDataResourcePageObject.adaptTo(ValueMap.class);

                      String itemtitle1=parentPageHandle.getTitle();
                    if(resPageVMap!=null && resPageVMap.get("analyticspagetitle", "")!=null && !resPageVMap.get("analyticspagetitle","").trim().equalsIgnoreCase("")){
                  itemtitle1=resPageVMap.get("analyticspagetitle", "");
                  }


					breadcrumbContent.append("<li itemprop='itemListElement' itemscope itemtype='http://schema.org/ListItem'><a itemprop='item'class='breadcrumblink' href='" + parentPath + "' itemtitle1='"+itemtitle1+"'><span itemprop='name'>"+ parentPageHandle.getTitle() + "</span></a><meta itemprop='position' content="+count+" /</li>");
				}
			}
             count =count+1; 
		}

		String[] selectorArray = slingRequest.getRequestPathInfo().getSelectors();

		if (selectorArray != null && selectorArray.length > 0 && selectorArray[0].equalsIgnoreCase(GlobalConstants.PRODUCT_TECH_SPEC_SELECTOR)) {
			productBookMarkLink = " > <a class='breadcrumblink' href='" + PathResolver.getShortURLPath(currentPage.getPath()) + "'><span itemprop='name'>"+ currentPage.getTitle() + "</span></a>" ;
		}
        }

	%> 

	<div class="breadcrumb-container">
		<div class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
            <ul>
			<%=breadcrumbContent%><%=productBookMarkLink%>
            </ul>
		</div>
	</div>
</c:if>
