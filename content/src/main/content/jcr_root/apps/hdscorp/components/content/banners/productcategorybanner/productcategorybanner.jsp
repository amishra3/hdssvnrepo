<%@page session="false"%>

<%@include file="/apps/foundation/global.jsp"%>

<%@page import="com.hdscorp.cms.util.PathResolver"%>
<%@page import="com.hdscorp.cms.util.PageUtils"%>


<c:set var="linkUrl" value="${properties.simplebannerbuttonurl}" />

<c:if test="${fn:startsWith(linkUrl,'/content/')}">
	<c:set var="linkUrl" value="${hdscorp:shortURL(linkUrl)}" />
</c:if>
<c:if test="${not empty properties.voverlay}">
	<c:set var="vid" value="${properties.simplebannerbuttonurl}" />
	<c:set var="vidurl"
		value="hds.resourceLib._openvideooverlayById(${vid});" />

</c:if>
<%-- <div class="common-hero-banner partner-program-banner clearfix" style="background-image: url('${properties.simplebannermagePath}');"> --%>
<div class="common-hero-banner partner-program-banner  clearfix rsImg"
	${hdscorp:bgImgAtrr(properties.simplebannermagePath,properties.simplebannermobileimagePath)}>
	<div class="common-hero-banner-container">
		<div class="col-lg-6 col-md-6 col-xs-12">
			<div class="top-banner-heading">
				<c:if test="${not empty properties.categoryiconpath}">
					<span class="icon"><img src="${properties.categoryiconpath}"
						alt='${empty properties.iconalttext?'' :properties.iconalttext}' title='${empty properties.icontitle?'' :properties.icontitle}'></span>
				</c:if>
				<c:choose>
					<c:when
						test="${(empty properties.categorytitletag) and (not empty properties.categorytitle) }">
						<h1 class="text">${properties.categorytitle}</h1>
					</c:when>
					<c:when
						test="${(not empty properties.categorytitletag) and (not empty properties.categorytitle) }">
						<c:set var="categorytitle"
							value="${fn:replace(properties.categorytitletag,'data',properties.categorytitle)}" />
            ${categorytitle}
    </c:when>
					<c:otherwise>

					</c:otherwise>
				</c:choose>
			</div>

			<c:choose>
				<c:when
					test="${(empty properties.simplebannertitletag) and (not empty properties.simplebannertitle) }">
					<h2 class="headline">${properties.simplebannertitle}</h2>
				</c:when>
				<c:when
					test="${(not empty properties.simplebannertitletag) and (not empty properties.simplebannertitle) }">
					<c:set var="simplebannertitle"
						value="${fn:replace(properties.simplebannertitletag,'data',properties.simplebannertitle)}" />
            ${simplebannertitle}
    </c:when>
				<c:otherwise>

				</c:otherwise>
			</c:choose>
			<h3>${properties.simplebannersubtitle}</h3>
			<h4 class="sub-headline">${properties.simplebannercontent}</h4>
			<c:if test="${not empty properties.simpllebannerbuttonlabel}">
				<div class="btn-square-white learn-more-white-link">
					<a class="animateLink" href="${properties.voverlay?'javascript:void(0);':linkUrl}"
						onclick="${!properties.voverlay?'':vidurl}"
						target="${properties.simplebannerurltargettype?'_blank':'_self'}">${properties.simpllebannerbuttonlabel}${not empty properties.thirdparty?' <span class="glyphicon glyphicon-new-window" aria-hidden="true"></span>':''}</a>
				</div>
			</c:if>
		</div>
	</div>
</div>

