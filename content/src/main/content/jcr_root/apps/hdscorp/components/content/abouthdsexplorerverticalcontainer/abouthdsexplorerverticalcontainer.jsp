<%@include file="/apps/foundation/global.jsp"%>
<%@page session="false" %>


  
<sling:adaptTo adaptable="${resource}" adaptTo="com.hdscorp.cms.slingmodels.AboutHDSExplorerVerticalContainerModel" var="aboutHDSExplorerVerticalContainerModel" />
<c:set var="colCss" value="col-sm-6"/>
<c:set var="rowCss" value=""/>
<c:choose>
    <c:when test="${fn:length(aboutHDSExplorerVerticalContainerModel.ahecList)==3}">
       <c:set var="colCss" value="col-sm-4"/>
    </c:when>
    <c:when test="${fn:length(aboutHDSExplorerVerticalContainerModel.ahecList)==2}">
       <c:set var="colCss" value="col-sm-6"/>
    </c:when>
    <c:when test="${fn:length(aboutHDSExplorerVerticalContainerModel.ahecList)==1}">
    <c:set var="rowCss" value="row-centered"/>
    <c:set var="colCss" value="col-sm-8 col-centered"/>
    </c:when>

   <c:otherwise>
</c:otherwise>
</c:choose>

<div class="about-hds-latest">
                <div class="content-container container-fluid">
                    <div class="row ${rowCss}">

<c:forEach items="${aboutHDSExplorerVerticalContainerModel.ahecList}" var="aheMultifield" varStatus="multfieldStatus">
    <c:forEach items="${aheMultifield}" var="aheMultiObject" varStatus="multiStatus">
        <div class="${colCss}">

    <c:choose>
    <c:when test="${fn:substringAfter(aheMultiObject.value, '$') == '1'}"> 
<h2><a class="animateLink" href="${fn:substringBefore(aheMultiObject.value, "$")}" target="_blank">${aheMultiObject.key}<span aria-hidden="true" class="glyphicon glyphicon-menu-right animateIcon"></span></a></h2>
    </c:when>
        <c:otherwise>
<h2><a class="animateLink" href="${fn:substringBefore(aheMultiObject.value, "$")}">${aheMultiObject.key}<span aria-hidden="true" class="glyphicon glyphicon-menu-right animateIcon"></span></a></h2>
        </c:otherwise>
    </c:choose>

 <cq:include path="abouthdspar${multfieldStatus.count}" resourceType="/apps/hdscorp/components/content/abouthdsverticalexplorer" />


 </div>
</c:forEach>

</c:forEach>

                    </div>
                </div>
            </div>