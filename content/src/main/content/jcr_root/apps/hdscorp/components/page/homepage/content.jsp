<%@include file="/apps/foundation/global.jsp"%>

	<c:set var="editbarstyle" value="" />
	<wcmmode:edit>
		<c:set var="editbarstyle" value="style='overflow:auto;'" />
	</wcmmode:edit>
	
	<div ${editbarstyle}>
		<cq:include path="herobannerpar" resourceType="hdscorp/components/content/banners/homeherobanner" />
		
	</div>

	<!-- CONTACT US PROMO STARTS Here-->
		
		
	<!-- CONTACT US PROMO ENDS Here-->
		
	<c:if test="${pageProperties.personalizationEnabled}">
		<cq:include path="clientcontext" resourceType="cq/personalization/components/clientcontext" />
	</c:if>
	
	<cq:include path="par" resourceType="foundation/components/parsys" />
