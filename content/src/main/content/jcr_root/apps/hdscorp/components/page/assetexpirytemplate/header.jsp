<%@include file="/apps/foundation/global.jsp"%>

<!-- HEADER STARTS -->
<div class="hds-global-header clearfix">
	
		<div class="header-container content-container">
			<a target="_blank" href="">
			<span class="hitachi-logo hidden-xs hidden-sm"></span>
			</a>
			 <a href="" target="_blank"><span
				class="hitachi-logo-mobile hidden-md hidden-lg"></span>
			</a>
			<!--Geo OverLay-->

		</div>
	
	<div class="hds-main-navigation-container">
		<div class="hds-main-navigation">
			<div class="col-md-3">
				<a href=""> <span
					class="sprite hitachi-sublogo-mobile hidden-sm hidden-md hidden-lg">Hitachi
						Data Systems</span><span class="sprite hitachi-sublogo hidden-xs">Hitachi
						Data Systems</span>
				</a>
			</div>

             

			<ul class="col-md-9 col-xs-12 removePosRelative globalNavWrapper  hidden-xs hidden-sm">
				
			</ul>
		</div>
	</div>
	
</div>

<!-- HEADER ENDS HERE -->

