<%--Brighttalk component.--%>
<%@include file="/apps/foundation/global.jsp"%>
<%@page session="false" %>
<%@page import="com.hdscorp.cms.util.PageUtils"%>
<%@page import="com.hdscorp.cms.constants.ServiceConstants"%> 
<%@page import="com.day.cq.search.result.SearchResult,com.day.cq.search.result.Hit,java.util.List"%>
<%@page import="com.hdscorp.cms.util.PropertyResolver"%>
<%@page import="com.hdscorp.cms.slingmodels.EventDataModel,java.util.ArrayList"%>
<%@page import="com.hdscorp.cms.dao.EventNode,com.hdscorp.cms.util.ViewHelperUtil,com.hdscorp.cms.search.SearchServiceHelper"%>
<%@page import="com.hdscorp.cms.util.PageUtils,sun.misc.BASE64Encoder"%>
<%@page import="com.hdscorp.cms.util.PathResolver"%>
<%@page import="com.day.cq.commons.Doctype,org.apache.commons.lang3.StringEscapeUtils,org.apache.sling.api.resource.ValueMap"%>
<%@page import="com.hdscorp.cms.restservice.ShortenerURLService"%>


<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<sling:adaptTo adaptable="${resource}" adaptTo="com.hdscorp.cms.slingmodels.EventDataModel" var="eventDataModel" />
<c:set var='brightalklookupath' value='${eventDataModel.brightalklookupath}' scope="application" />
<c:set var="previewImagePath" value="${properties.esbrightalkdefaultpreviewimage}" scope="application"/>

<div class="col-md-9 newsWrapper">
    <div class="noEventFilter">${eventDataModel.noeventfoundMsg} </div>    
    <c:forEach items="${eventDataModel.eventFinalNodesData}" var="eventAlldata">
        <div class="newsWrapper-listing">
            <div class="events_months">                
                <h2>${eventAlldata.key}</h2></div>
            <c:forEach items="${eventAlldata.value}" var="eventNodes">
      <c:choose>
         <c:when test="${not empty eventNodes.previewImagePath}">
            <c:set var="previewImagePath" value="${eventNodes.previewImagePath}"/>
         </c:when>
          <c:otherwise>
            <c:set var="previewImagePath" value="${properties.esbrightalkdefaultpreviewimage}"/>
          </c:otherwise>
                      </c:choose>
 			<c:set var="path" value="<%=request.getRequestURL().toString()%>" />
		<%
   			 ValueMap valueMap = currentPage.getProperties();
 			String shortPath = PathResolver.getShortURLPath(pageContext.getAttribute("path").toString());
			String domainVal = (String)pageProperties.getInherited("domain", "");
			String absolutePath = PathResolver.getAbsoluteDomainUrl(shortPath, domainVal);
			pageContext.setAttribute("absolutePath", absolutePath);
		%>
                <c:choose>
                    <c:when test="${eventNodes.isWebcast=='true'}">

                                         <div data-events="hdscorp:events/webcasts"  data-startDate="${eventNodes.eventStartDate}" data-endDate="" data-region="hdscorp:events/webcasts" class="newsEvents event-webcast scrollto${eventNodes.communicationId}" style="display: block;">
                                            <h3>${eventNodes.eventTitle}</h3>
                                            <p>${eventNodes.eventStartDate}</p>
                                            <small>${eventNodes.duration}</small>
                                            <h4 class="author">${eventNodes.author}</h4>
                                            <p>${eventNodes.summary}<br><br><small>UPCOMING</small></p>
                                             <div class="WebcastDetails">
                                                 <img class="img-responsive" src="${previewImagePath}" alt=""/>
                                                <a rel="${not empty playermodal?'modal':'dummy'}" href="${eventNodes.herfLink}" target="_blank" title="Play" comid="${eventNodes.communicationId}" class="playvideo">${registernow} <span class="glyphicon" aria-hidden="true"></span></a>
                                            </div>
                                                <div class="expandMe less"><span class="glyphicon glyphicon-plus-sign"></span>${detailslabel}</div>
                                        </div>	

                        <script type="application/ld+json">  {
                  "@context": "http://schema.org",
                  "@type": "Event",
                  "description":"${fn:substring(eventNodes.summary, 0, 200)}",    
                  "name": "${eventNodes.eventTitle}",
                  "startDate" : "${eventNodes.eventStartDate}",
                  "endDate" : "${eventNodes.eventStartDate}",
                  "url": "${absolutePath}#tab=event#comid=${eventNodes.communicationId}",
                  "location" : {
                     	  "@type" : "Place",
                       	 "name" : "Webinar",
                   		 "address" : "Online"
                  }
                }
		</script>
                    </c:when>
                    <c:otherwise>				
                <div data-events="${eventNodes.eventTyptagId}" 
                data-startDate="${eventNodes.eventStartDate}" 
                data-endDate="${eventNodes.eventEndDate}" 
                data-region="${eventNodes.eventRegiontagId}"
                data-event-id="${eventNodes.eventId}" 
                class="newsEvents">
                    <small> ${eventNodes.eventTyptagName}</small> 
                    
                    <h3>${eventNodes.eventTitle} </h3>
                    <p>${eventNodes.eventStartDate}&ndash;${eventNodes.eventEndDate}: ${eventNodes.eventLocation}</p>
					<div class="eventDetails" style="background-image: url('${eventNodes.eventImageBackground}')">
                        <div class="row">
                            <div class="col-md-12">
                                ${eventNodes.eventDescription}
								<div class="btn-square-white request">                                    
    								<a href="${eventNodes.eventRegisterNowLink}" title="${eventNodes.eventRegisterNowLabel}" target="${eventNodes.newwindow?'_blank':'_self'}">${eventNodes.eventRegisterNowLabel}${eventNodes.thirdpartyicon?' <span class="glyphicon glyphicon-new-window" aria-hidden="true"></span>':''}</a> 
                                </div>

                            </div>                            
                        </div>
                    </div>
                    <div class="expandMe less" href="javascript:void(0);"><span class="glyphicon glyphicon-plus-sign"></span>${eventDataModel.detailsLabel} </div>
                </div>

                        <c:set var="eventscemaval" value="${eventNodes.eventDescription}" scope="request" />
                        <%
            				String escaped = request.getAttribute("eventscemaval").toString().replaceAll("\\<.*?>","");
							pageContext.setAttribute("escaped", escaped);

            			%>
                        <script type="application/ld+json">
                {
                  "@context": "http://schema.org",
                  "@type": "Event",
                      "description":"${fn:substring(escaped, 0, 200)}",    
                  "name": "${eventNodes.eventTitle}",
                  "startDate" : "${eventNodes.eventStartDate}",
                  "endDate" : "${eventNodes.eventEndDate}",
                      "url": "${absolutePath}#event-id=${eventNodes.eventId}",
                  "location" : {
                     	 "@type" : "Place",
                       	 "name" : "${eventNodes.eventLocation}",
                   		 "address" : "${eventNodes.eventLocation}"
                  }
                }
                </script>
                    </c:otherwise>
                      </c:choose> 

            </c:forEach>
            <!--/ All Events are Loaded here -->
        </div>
    </c:forEach>   
    								<div id="loadMoreMonth" class="btn-square-red load-more-link">
										<a title="See Next 3 Months of Events"
                                        href="javascript:void(0);">${evemodval}</a>
									</div>

</div>