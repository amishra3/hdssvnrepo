<%--
 DAM PDF Listing Component.
--%>
<%@page session="false" import="com.hdscorp.cms.handlers.PdfListingHandler,java.util.*"%>
<%@include file="/apps/foundation/global.jsp"%>
<cq:includeClientLib css="hds.assetexpiry" />

<%
    String domainVal = (String)pageProperties.getInherited("domain", "");
    PdfListingHandler pdfList = new PdfListingHandler();
	List<Map<String,String>> pdfDetails = pdfList.searchAllPDFAssets(slingRequest, properties,domainVal);
if(pdfDetails != null){
	pageContext.setAttribute("pdfList", pdfDetails);    
}

%>
<c:set var="pdfList" value="${pdfList}" />
<div class="hds-contactspr">
    <div class="content-container">
        <div class="hds-pr-contact-list">
            <div class="row">
                <div class="col-sm-12 col-no-pad">
                    <div class="pr_cnlisting ">                        
                        <ul>
                            <c:forEach var="pdfdetails" items="${pdfList}">
                                <c:if test="${not empty pdfdetails.path}">
                                <li><a href="${pdfdetails.path}" target="_blank">${pdfdetails.path}</a></li>
                                </c:if>
                            </c:forEach>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="scroll-down">
    <a class="ico-arrow-hm" href="javascript:void(0);"><span class="sprite icon-arrow-down"></span></a>
</div>

<wcmmode:edit>
    <p>
        <span class="cq-text-placeholder-ipe">Configure DAM PDF Listing Setup</span>
    </p>
</wcmmode:edit>


