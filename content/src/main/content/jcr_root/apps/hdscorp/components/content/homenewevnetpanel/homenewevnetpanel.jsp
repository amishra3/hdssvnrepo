<%--Home Event and News component Panel.--%>

<%@include file="/apps/foundation/global.jsp"%>
<%@page session="false" %>

<sling:adaptTo adaptable="${resource}" adaptTo="com.hdscorp.cms.slingmodels.HomeNewsEventPanelModel" var="homeNewsEventPanel" />

<div class="home-news-event-panel">
    <div class="content-container container-fluid">
        <div class="row">
            <div class="col-md-12"><h2>${homeNewsEventPanel.headingLabel}</h2></div>
            <div class="col-md-6">
                <img src="${homeNewsEventPanel.imagePath}" alt="" class="img-responsive">
            </div>
            <div class="col-md-6">
                <div class="date">${homeNewsEventPanel.itemdate}</div>
                <div class="title">${homeNewsEventPanel.title}</div>
                <div class="desc">
                    <p>${homeNewsEventPanel.description}</p>
                </div>
                <div class="read-more btn-square-red"><a href="${homeNewsEventPanel.ctaLink}" >${homeNewsEventPanel.ctaLabel}</a></div>
            </div>
        </div>
    </div>
</div>
