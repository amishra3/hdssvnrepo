<%@include file="/apps/foundation/global.jsp"%>
<%@page session="false"%>
<sling:adaptTo adaptable="${resource}"
	adaptTo="com.hdscorp.cms.slingmodels.NewsInsightExplorerVerticalContainerModel"
	var="newsInsightContainerModel" />
<c:set var="colCss" value="col-sm-4"/>
<c:choose>
	<c:when test="${fn:length(newsInsightContainerModel.nivcList)==3}">
		<c:set var="colCss" value="col-sm-4" />
	</c:when>
	<c:when test="${fn:length(newsInsightContainerModel.nivcList)==2}">
		<c:set var="colCss" value="col-sm-6" />
	</c:when>
	<c:when test="${fn:length(newsInsightContainerModel.nivcList)==1}">
		<c:set var="rowCss" value="row-centered"/>
    <c:set var="colCss" value="col-sm-8 col-centered"/>
	</c:when>

	<c:otherwise>
	</c:otherwise>
</c:choose>
<div class="news-insight-explore">
	<div class="content-container container-fluid">
<div class="row ${rowCss}">
		<c:forEach items="${newsInsightContainerModel.nivcList}" var="nivcMultifield" varStatus="multfieldStatus">
			<c:forEach items="${nivcMultifield}" var="nivcMultiObject" varStatus="multiStatus">
                <div class="${colCss}">

					<c:set var="targetlink" value="${fn:substringBefore(nivcMultiObject.value, '$')}"/>
                    <c:set var="newwin" value="${fn:substringAfter(nivcMultiObject.value, '$')}"/>
					<c:if test="${fn:startsWith(targetlink,'/content/')}">
						<c:set var="targetlink" value="${hdscorp:shortURL(targetlink)}" />
					</c:if>
					<h2><a class="animateLink" href="${targetlink}" target="${newwin==1?'_blank':'_self'}">
						${nivcMultiObject.key}
							<span aria-hidden="true" class="glyphicon glyphicon-menu-right animateIcon"></span>						
					</a></h2>
					 <c:set var="eventLandingURL" value="${hdscorp:shortURL(fn:substringBefore(nivcMultiObject.value,'$'))}" scope="request"/>               
					<cq:include path="firstpar${multfieldStatus.count}" resourceType="/apps/hdscorp/components/content/newsinsightverticalexplorer" />
				</div>
			</c:forEach>

		</c:forEach>
<div class="orange-sep">&nbsp;</div>
	</div>
    </div>
</div>