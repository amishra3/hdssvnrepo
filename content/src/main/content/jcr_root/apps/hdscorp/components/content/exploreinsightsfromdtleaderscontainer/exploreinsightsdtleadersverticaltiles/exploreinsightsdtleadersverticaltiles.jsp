<%@include file="/apps/foundation/global.jsp"%>
<%@page session="false"%>
<%@page import="com.hdscorp.cms.util.PageUtils"%>
<sling:adaptTo adaptable="${resource}"
	adaptTo="com.hdscorp.cms.slingmodels.ExploreInsigtsDTLeadersVerticalTilesModel"
	var="exploreInsigtsDTLeadersVerticalTilesModel" />
   
<c:if test="${not empty exploreInsigtsDTLeadersVerticalTilesModel.eidtLabelTargetURL}">
		<c:set var="linkUrl" value="${hdscorp:shortURL(exploreInsigtsDTLeadersVerticalTilesModel.eidtLabelTargetURL)}" />
	</c:if>
<div class="news-insight-explore-spotlight"
	style="background-image: url('${exploreInsigtsDTLeadersVerticalTilesModel.eidtBGImagePath}')">
	<div class="spotlight-mobile hidden-md hidden-lg"></div>
	<div class="spotlight-content">

           <div class="spotlight-title">${exploreInsigtsDTLeadersVerticalTilesModel.eidtBlogTilte}
			</div>
		<div class="read-more">
            <c:choose> 
				<c:when test="${exploreInsigtsDTLeadersVerticalTilesModel.eidttOpenInNewwindow && not empty linkUrl}">
					<a href="${linkUrl}"
						target="_blank" class="animateLink">${exploreInsigtsDTLeadersVerticalTilesModel.eidtBlogDescription}${properties.eidtthirdpartytop?' <span class="glyphicon glyphicon-new-window" aria-hidden="true"></span>':' <span class="glyphicon glyphicon-menu-right animateIcon" aria-hidden="true"></span>'}</a>
				</c:when>
                <c:when test="${ empty exploreInsigtsDTLeadersVerticalTilesModel.eidtLabelTargetURL}">
					${exploreInsigtsDTLeadersVerticalTilesModel.eidtBlogDescription}
				</c:when>
				<c:otherwise>
					<a href="${linkUrl}"
						class="animateLink">${exploreInsigtsDTLeadersVerticalTilesModel.eidtBlogDescription}<span
						class="glyphicon glyphicon-menu-right animateIcon"
						aria-hidden="true"></span></a>
				</c:otherwise>
			</c:choose>

		</div>
	</div>
</div>

<c:if test="${not empty exploreInsigtsDTLeadersVerticalTilesModel.eidtLabelBTargetURL}">
		<c:set var="linkBottomUrl" value="${hdscorp:shortURL(exploreInsigtsDTLeadersVerticalTilesModel.eidtLabelBTargetURL)}" />
	</c:if>

<div class="news-insight-explore-spotlight spotlight-normal">
	<div class="spotlight-content">
		<div class="icon hidden-xs">
			<img src="${exploreInsigtsDTLeadersVerticalTilesModel.eidtIconBImagePath}" alt="${properties.eidticonbimagealttext}"
				title="${properties.eidticonbimagetitle}">
		</div>
		<div class="icon hidden-sm hidden-md hidden-lg">
			<img src="${exploreInsigtsDTLeadersVerticalTilesModel.eidtIconBImagePath}" alt="${properties.eidticonbimagealttext}"
				title="${properties.eidticonbimagetitle}">
		</div>
		<div class="type">${exploreInsigtsDTLeadersVerticalTilesModel.eidtIconBImageLabel}
			</div>
		<div class="spotlight-title">${exploreInsigtsDTLeadersVerticalTilesModel.eidtBBlogBescription}</div>
		<div class="read-more">
			<c:choose>
				<c:when test="${exploreInsigtsDTLeadersVerticalTilesModel.eidttOpenBInNewwindow && not empty linkBottomUrl}">
					<a href="${linkBottomUrl}"
						target="_blank" class="animateLink">${exploreInsigtsDTLeadersVerticalTilesModel.eidtIconReadmoreLabel}${properties.eidtthirdparty?' <span class="glyphicon glyphicon-new-window" aria-hidden="true"></span>':' <span class="glyphicon glyphicon-menu-right animateIcon" aria-hidden="true"></span>'}</a>
				</c:when>
				<c:otherwise>
                    <c:if test="S{not empty linkBottomUrl }">
					<a href="${linkBottomUrl}"
						class="animateLink">${exploreInsigtsDTLeadersVerticalTilesModel.eidtIconReadmoreLabel}${properties.eidtthirdparty?' <span class="glyphicon glyphicon-new-window" aria-hidden="true"></span>':' <span class="glyphicon glyphicon-menu-right animateIcon" aria-hidden="true"></span>'}</a>
                    </c:if>
				</c:otherwise>
			</c:choose>

		</div>

	</div>
</div>