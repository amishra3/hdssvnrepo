<%--
  assetexpiryconfig Component.
--%>

<%@page session="false"%>
<%@include file="/apps/foundation/global.jsp"%>

<%@page import="com.hdscorp.cms.util.PathResolver"%>
<%@page import="com.hdscorp.cms.util.PageUtils"%>

<%@ page import="org.apache.sling.commons.json.JSONObject" %>
<%@ page import="java.io.*" %>
<%@ page import="java.io.PrintWriter" %>
<%@ page import="org.apache.sling.commons.json.JSONArray" %>
<cq:includeClientLib css="hds.assetexpiry" />


<c:choose>
	<c:when test="${not empty properties.expiryRootConfig}">

<div class="hds-contactspr">
                <div class="content-container">
                    <div class="hds-pr-contact-list">
                        <div class="row">
						   	<div class="col-sm-12 col-no-pad">
                            	<div class="pr_cnlisting ">
                                	<h1><strong>${properties.assetExpiryTitle}</strong></h1>
                                </div>
                            </div>
							 <div class="col-sm-12 col-no-pad main_contacts">
                            	<div class="pr_cnlisting last">
<%
    try {
        Property property = null;

        if (currentNode.hasProperty("expiryRootConfig")) {
            property = currentNode.getProperty("expiryRootConfig");
        }

        if (property != null) {
            JSONObject obj = null;
            Value[] values = null;

            if (property.isMultiple()) {
                values = property.getValues();
            } else {
                values = new Value[1];
                values[0] = property.getValue();
            }

            for (Value val : values) {
                obj = new JSONObject(val.getString());
%>
    <h2><strong>Dam Asset Path: </strong> <%= obj.get("damFolderPath") %></h2>
    <h4><strong>Email Template Path: </strong> <%= obj.get("emailTemplatePath") %></h4>
<%
                JSONObject cs = null;

                if(obj.get("emailid") != null){
                    JSONArray region = new JSONArray(String.valueOf(obj.get("emailid")));
%>

    <ul>
        <%
           for(int r = 0; r < region.length(); r++){
           cs = new JSONObject(String.valueOf(region.get(r)));
         %>
        <li>Email Id: <a><%= cs.get("emailId") %></a></li>
        <%
           }
             }
         %>
     </ul>
		<% }
     		} else { }
    
    } catch (Exception e) {
        out.println("Exception is: " + e);
    }
%>

	

                                </div>
                            </div>
                            <div class="col-sm-12 col-no-pad">
                                <div class="pr_cnlisting ">
                                    <h2>Email Alert Notification Setup:</h2>

                                    <ul>
    									<%	boolean emailAlertOne = properties.get("emailAlertOne", false);                                     	
    										boolean emailAlertFifteen = properties.get("emailAlertFifteen", false);
											boolean emailAlertThirty = properties.get("emailAlertThirty", false);
											boolean emailAlertSixty = properties.get("emailAlertSixty", false);
											boolean emailAlertNinety = properties.get("emailAlertNinety", false);

										if(emailAlertOne == true){ %>
                                        <li>Email Alert Before 1 days: <strong style="color:green;">Enabled</strong></li>
                                        <% }else { %>
                                        <li>Email Alert Before 1 days: <strong style="color:red;">Disabled</strong></li>
                                        <% }
										if(emailAlertFifteen == true){ %>
                                        <li>Email Alert Before 15 days: <strong style="color:green;">Enabled</strong></li>
                                        <% }else { %>
                                        <li>Email Alert Before 15 days: <strong style="color:red;">Disabled</strong></li>
                                        <% } 
                                        if(emailAlertThirty == true){ %>
                                        <li>Email Alert Before 30 days: <strong style="color:green;">Enabled</strong></li>
                                        <% }else { %>
                                        <li>Email Alert Before 30 days: <strong style="color:red;">Disabled</strong></li>
                                        <% }
                                        if(emailAlertSixty == true){ %>
                                        <li>Email Alert Before 60 days: <strong style="color:green;">Enabled</strong></li>
                                        <% }else { %>
                                        <li>Email Alert Before 60 days: <strong style="color:red;">Disabled</strong></li>
                                        <% }
                                        if(emailAlertNinety == true){ %>
                                        <li>Email Alert Before 90 days: <strong style="color:green;">Enabled</strong></li>
                                        <% }else { %>
                                        <li>Email Alert Before 90 days: <strong style="color:red;">Disabled</strong></li>
                                        <% } %>


                                    </ul>
                                </div>
                            </div>
							
							
                         </div>
                    </div>
                </div>
            </div>
            <div class="scroll-down">
                <a class="ico-arrow-hm" href="javascript:void(0);"><span class="sprite icon-arrow-down"></span></a>
            </div>


</c:when>
	<c:otherwise>
		<wcmmode:edit>
			<p>
				<span class="cq-text-placeholder-ipe">Configure Asset Expiry Notification Setup</span>
			</p>
		</wcmmode:edit>
	</c:otherwise>
</c:choose>  
