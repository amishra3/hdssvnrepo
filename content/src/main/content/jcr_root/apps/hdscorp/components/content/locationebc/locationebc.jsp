<%@include file="/apps/foundation/global.jsp"%>
<%@page session="false"%>
<%@page import="com.hdscorp.cms.util.PageUtils"%>
 
<c:set var="locationEBCList"
    value="<%=PageUtils.convertMultiWidgetToList(properties,"locebctitle-locebcbgimage-locbgimagealt-locebclink-locebcopeninnewwindow-locebcdirectiondetail-locebcaddress-locebcbgimagemobi-locebcdirectiondetaillink")%>" />
 
<div class="location-centers">
    <div class="content-container container-fluid">
         <h2 class="grey-heading">${properties.locebcheadingtitle}</h2> 
                 <div class="row"> 
                        <c:forEach items="${locationEBCList}" var="locationEBC">
                                 <div class="col-sm-6 col-centered">
                                    <c:choose>
                    					<c:when test="${not empty locationEBC.locebclink}">
											 <h3><a target="${locationEBC.locebcopeninnewwindow==1?'_blank':'_self'}"href="${locationEBC.locebclink}" class="animateLink">${locationEBC.locebctitle}${locationEBC.locebcopeninnewwindow==1?' <span class="glyphicon glyphicon-new-window" aria-hidden="true"></span>':' <span class="glyphicon glyphicon-menu-right animateIcon" aria-hidden="true"></span>'}</a></h3>                                  
									</c:when>
                   					 <c:otherwise>
											<h3>${locationEBC.locebctitle}</h3>
									</c:otherwise>
                   	 			  </c:choose> 

                                    <div style="background-image: url('${locationEBC.locebcbgimage}')" class="location-centers-box">
                                       <div class="hidden-sm hidden-md hidden-lg">
                                          <img class="img-responsive" alt="${locationEBC.locbgimagealt}" src="${locationEBC.locebcbgimagemobi}">
                                       </div>
                                       <div class="location-centers-content">                                   
                                          <div class="clearfix"></div>
                                          <div class="title first">
                                              <p>${locationEBC.locebcaddress}</p>
                                             <a class="animateLink" target="_blank" href="${locationEBC.locebcdirectiondetaillink}">${locationEBC.locebcdirectiondetail} <span aria-hidden="true" class="glyphicon glyphicon-new-window"></span></a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                        </c:forEach>
                </div>
 
    </div>
</div>